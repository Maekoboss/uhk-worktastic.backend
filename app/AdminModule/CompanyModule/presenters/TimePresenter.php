<?php

    namespace AdminModule\CompanyModule;

    /**
     * @persistent(listingGeneralSessions, listingGeneralReports)
     */
    class TimePresenter extends BasePresenter {

        /** @persistent */
        public $sessionID;

        /** @var \Nette\Database\Table\ActiveRow */
        private $session;

        // Tabs settings
        public function createComponentMenuTabs() {
            $action = $this->getAction();
            switch ($action) {
                default :
                    $tabs = array(
                        'Sessions' => 'default',
                        'Users' => 'users',
                        'Report' => 'report'
                    );
                    break;
            }
            return new \AdminModule\VisualComponents\MenuTabs($tabs, $this->translator);
        }

        // ############################################ Sessions ############################################
        public function createComponentFormContentSession() {
            $modulesFlow = array(
                'sessions' => array(
                    'model' => $this->context->getService('sessions'),
                    'id' => $this->session != null ? $this->session->id : 'empty',
                    'redirect' => true,
                    'redirectMask' => 'session',
                    'settings' => array(
                        'lang' => false,
                        'main_key' => false
                    )
                )
            );

            $staticInputs = array(
                array(
                    'module' => 'sessions',
                    'main_key' => 'data',
                    'column' => 'app_start',
                    'type' => 'date',
                    'title' => 'App Start',
                    'required' => true
                ),
                array(
                    'module' => 'sessions',
                    'main_key' => 'data',
                    'column' => 'app_stop',
                    'type' => 'date',
                    'title' => 'App Stop',
                    'required' => true
                ),
                array(
                    'module' => 'sessions',
                    'main_key' => 'data',
                    'column' => 'start',
                    'type' => 'date',
                    'title' => 'Start Date',
                    'required' => false
                ),
                array(
                    'module' => 'sessions',
                    'main_key' => 'data',
                    'column' => 'stop',
                    'type' => 'date',
                    'title' => 'Stop Date',
                    'required' => false
                ),
            );

            return new \AdminModule\VisualComponents\FormContent($staticInputs, 'simple', $modulesFlow, array('listingGeneralSessions'));
        }

        public function createComponentListingGeneralSessions() {
            $users = $this->database->table('users')->where(array('role' => 'registered', 'companies_id', $this->user->identity->companies_id))->fetchPairs('id', 'name');
            $items = array(
                'where' => array(
                    'companies_id' => $this->user->identity->companies_id,
                    '(app_start IS NOT NULL OR start IS NOT NULL) AND (app_stop IS NOT NULL OR stop IS NOT NULL)'
                ),
                'order' => 'app_start DESC, start DESC',
                'columns' => array(
                    array(
                        'data' => 'userName',
                        'appearance' => 'text',
                        'title' => 'User'
                    ),
                    array(
                        'data' => 'start',
                        'columns' => array('app_start', 'start'),
                        'appearance' => 'sessionDate',
                        'title' => 'Start Date'
                    ),
                    array(
                        'data' => 'stop',
                        'columns' => array('app_stop', 'stop'),
                        'appearance' => 'sessionDate',
                        'title' => 'Stop Date'
                    ),
                    array(
                        'data' => 'duration',
                        'appearance' => 'text',
                        'title' => 'Duration'
                    ),
                )
            );
            $paginator = array(
                'type' => 'endless'
            );
            $filters = array(
                'users_id' => array(
                    'type' => 'dropdown',
                    'options' => $users,
                    'prompt' => 'User'
                )
            );
            $tools = array(
                'edit' => array(
                    'icon' => 'fa fa-pencil-square-o',
                    'link' => array(
                        'type' => 'modal',
                        'modalID' => 'formContentSession',
                        'presenter' => 'this',
                        'redirectMask' => 'session'
                    )
                ),
                'delete' => array(
                    'snippets' => array('content')
                )
            );

            return new \AdminModule\VisualComponents\ListingGeneralSessions($this->context->getService('sessions'), $items, $paginator, $tools, null, $filters, 'Sessions');
        }

        // ############################################ Users ############################################

        public function createComponentListingGeneralUsers() {
            $items = array(
                'where' => array(
                    'companies_id' => $this->user->identity->companies_id,
                    'role' => 'registered',
                ),
                'order' => 'id',
                'columns' => array(
                    array(
                        'appearance' => 'iterator',
                        'title' => '#'
                    ),
                    array(
                        'column' => 'name',
                        'appearance' => 'text',
                        'title' => 'Name'
                    ),
                    array(
                        'column' => 'email',
                        'appearance' => 'text',
                        'title' => 'Email'
                    )
                )
            );
            $paginator = array(
                'type' => 'endless'
            );
            $tools = array(
                'detache' => array(
                    'snippets' => array('content')
                )
            );

            return new \AdminModule\VisualComponents\ListingGeneral($this->context->getService('users'), $items, $paginator, $tools);
        }

        // ############################################ Report ############################################

        public function createComponentListingGeneralReports() {
            $items = array(
                'where' => array(
                    'companies_id' => $this->user->identity->companies_id
                ),
                'order' => 'users_id',
                'columns' => array()
            );
            $filters = array(
                'app_start' => array(
                    'type' => 'month'
                )
            );

            return new \AdminModule\VisualComponents\ListingGeneralReport($this->context->getService('sessions'), $items, null, null, null, $filters, 'Report');
        }

        // ############################################ Actions and pages navigation ############################################
        public function startup() {
            parent::startup();

            // Main item ID
            $this->sessionID = $this->getParameter('sessionID');
            if ($this->sessionID !== null) {
                $this->session = $this->context->getService('sessions')->findById($this->sessionID);
            } else {
                $this->session = null;
            }
            $this->template->session = $this->session;
            if ($this->isAjax()) {
                $this->redrawControl('formContentSession');
            }
        }

    }
    