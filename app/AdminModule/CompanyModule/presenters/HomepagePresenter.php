<?php

namespace AdminModule\CompanyModule;

class HomepagePresenter extends BasePresenter {
    
    /** @persistent */
    public $companyID;
    
    /** @var \Nette\Database\Table\ActiveRow */
    private $company;
    
    // Tabs settings
    public function createComponentMenuTabs() {
	$action = $this->getAction();
	switch ($action) {
	    default :
		$tabs = array(
		    'Company Data' => 'default'
		);
		break;
	}

	return new \AdminModule\VisualComponents\MenuTabs($tabs, $this->translator);
    }
    
    // ############################################ Company info ############################################   
    public function createComponentFormContent() {
	$modulesFlow = array(
	    'companies' => array(
		'model' => $this->context->getService('companies'),
		'id' => $this->company->id,
                'settings' => array(
                    'main_key' => false,
                    'lang' => false
                ),
		'children' => array(
                    'gallery' => array(
			'model' => $this->context->getService('gallery')
		    )
		)
	    ),
	);

	$staticInputs = array(
	    array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'name',
		'type' => 'text',
		'title' => 'Company Name',
		'required' => true
	    ),
            array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'company_id',
		'type' => 'text',
		'title' => 'Company ID',
		'required' => true
	    ),
            array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'vat_id',
		'type' => 'text',
		'title' => 'Company VAT ID',
		'required' => true,
                'lineAfter' => true
	    ),
            array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'street',
		'type' => 'text',
		'title' => 'Street',
		'required' => true
	    ),
            array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'city',
		'type' => 'text',
		'title' => 'City',
		'required' => true
	    ),
            array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'zip',
		'type' => 'text',
		'title' => 'Zip',
		'required' => true
	    ),
            array(
		'module' => 'companies',
                'main_key' => 'data',
                'column' => 'country',
		'type' => 'text',
		'title' => 'Country',
		'required' => true
	    ),          
            array(
		'module' => 'gallery',
                'main_key' => 'logo',
                'column' => 'file_name',
		'type' => 'image',
		'title' => 'Logo',
		'required' => false
	    ),
	);
	$control = new \AdminModule\VisualComponents\FormContent($staticInputs, 'simple', $modulesFlow);
	return $control;
    }

    // ############################################ Actions and pages navigation ############################################
        public function startup() {
            parent::startup();

            // Company ID
            $this->companyID = $this->user->getIdentity()->companies_id;
            if ($this->companyID !== null) {
                $this->company = $this->context->getService('companies')->findById($this->companyID);
            } else {
                $this->company = null;
            }
            $this->template->company = $this->company;
        }

}
