<?php

namespace AdminModule\VisualComponents;

use Nette\Application\UI,
    Nette\ComponentModel\IContainer,
    Nette\Utils\Strings;

class FormContent extends UI\Control {

    private $saveButtons;
    // nastaveni modelu
    private $models = array();
    private $snippets = array();
    // nastaveni poli pro editaci
    private $staticInputs;
    private $groupedInputs;
    private $redirectID;

    public function __construct($staticInputs, $saveButtons, $modulesFlow, $snippets = array('content')) {
        $this->staticInputs = $staticInputs;

        // Dynamicke nacitani modelu	
        $this->models = $modulesFlow;

        // Seznam snippetu, ktere se refreshnou po dokonceni pozadavku
        $this->snippets = $snippets;

        // groupnuti inputu podle modulu a main_key
        $position = 0;
        foreach ($this->staticInputs as $inputKey => $data) {
            $this->groupedInputs[$data['module']][$data['main_key']][$position] = $data;
            $position++;
        }

        // Loadnuti save butons
        $this->saveButtons = $saveButtons;

        // Loadnuti dat do formulare
        $this->getForContentForm($this->models);
    }

    public function getForContentForm($modulesFlow, $parentModule = null) {
        foreach ($modulesFlow as $moduleKey => $modelData) {
            if (array_key_exists($moduleKey, $this->groupedInputs)) {
                $modelData['id'] = isset($modelData['id']) ? $modelData['id'] != null ? $modelData['id'] : null : null;
                if ($parentModule == null || isset($moduleData['parent_module'])) {
                    $modelData['parent_id'] = isset($modelData['parent_id']) ? $modelData['parent_id'] : null;
                    $modelData['parent_id_row'] = isset($modelData['parent_module']) ? $modelData['parent_module'] . '_id' : null;
                } else {
                    $modelData['parent_id'] = $parentModule['id'];
                    $modelData['parent_id_row'] = $parentModule['name'] . '_id';
                }

                $inputs = $modelData['model']->getForContentForm($modelData['parent_id'], $modelData['parent_id_row'], $this->groupedInputs[$moduleKey], $modelData['id'], (!empty($modelData['settings']) ? $modelData['settings'] : null));
                if ($inputs) {
                    foreach ($inputs as $mainKey => $inputsData) {
                        foreach ($inputsData as $position => $input) {
                            if (isset($input['value'])) {
                                $this->staticInputs[$position]['value'] = $input['value'];
                            } else {
                                unset($this->staticInputs[$position]['value']);
                            }

                            if (isset($input['id'])) {
                                $this->groupedInputs[$moduleKey][$mainKey][$position]['id'] = $input['id'];
                                $this->staticInputs[$position]['id'] = $input['id'];
                            } else {
                                unset($this->groupedInputs[$moduleKey][$mainKey][$position]['id']);
                                unset($this->staticInputs[$position]['id']);
                            }
                        }
                    }
                }
            }
            if (array_key_exists('children', $modelData)) {
                $parentInfo = array(
                    'name' => $moduleKey,
                    'id' => isset($modelData['id']) ? $modelData['id'] : null
                );
                $this->getForContentForm($modelData['children'], $parentInfo);
            }
        }
    }

    public function createComponentContentForm() {
        $form = new UI\Form();
        $form->setTranslator($this->presenter->translator);

        if ($this->staticInputs) {
            foreach ($this->staticInputs as $inputKey => $input) {
                if ($input['type'] == 'text') {
                    if (isset($input['appearance'])) {
                        if ($input['appearance'] == 'dropdown') {
                            $formInput = $form->addSelect($inputKey, $input['title'], $input['options']);
                            if (isset($input['prompt'])) {
                                $formInput->setPrompt($input['prompt']);
                            }
                        } elseif ($input['appearance'] == 'hidden') {
                            $formInput = $form->addHidden($inputKey, $input['value']);
                        }
                    } else {
                        $formInput = $form->addText($inputKey, $input['title']);
                    }
                } elseif ($input['type'] == 'textArea') {
                    $formInput = $form->addTextArea($inputKey, $input['title'])
                        ->setAttribute('data-securedupload', $this->link('safeUpload!'))
                        ->setAttribute('data-secureddelete', $this->link('safeDelete!'));
                } elseif ($input['type'] == 'date') {
                    $formInput = $form->addDateTimePicker($inputKey, $input['title'], 16, 16)
                        ->setAttribute('data-lang', $this->presenter->backLang);
                } elseif ($input['type'] == 'bool') {
                    $switcher = array(
                        '0' => $this->presenter->translator->translate('no'),
                        '1' => $this->presenter->translator->translate('yes'),
                    );
                    $formInput = $form->addRadioList($inputKey, $input['title'], $switcher);
                } elseif ($input['type'] == 'image') {
                    $formInput = $form->addUpload($inputKey, $input['title']);
                    if ($input['required'] == true && !isset($input['value'])) {
                        $formInput->addRule(UI\Form::FILLED, 'field-is-mandatory')
                                ->addRule(UI\Form::IMAGE, 'supported-formats-are-jpg-png-gif')
                                ->addRule(UI\Form::MAX_FILE_SIZE, 'max-file-size-is*NumberFix*', 5 * 1024 * 1024);
                    } else {
                        $formInput->addCondition(UI\Form::FILLED)
                                ->addRule(UI\Form::IMAGE, 'supported-formats-are-jpg-png-gif')
                                ->addRule(UI\Form::MAX_FILE_SIZE, 'max-file-size-is*NumberFix*', 5 * 1024 * 1024);
                    }
                } elseif ($input['type'] == 'file') {
                    $formInput = $form->addUpload($inputKey, $input['title']);
                    if ($input['required'] == true && !isset($input['value'])) {
                        $formInput->addRule(UI\Form::FILLED, 'field-is-mandatory')
                                ->addRule(UI\Form::MAX_FILE_SIZE, 'max-file-size-is*NumberFix*', 5 * 1024 * 1024);
                    } else {
                        $formInput->addCondition(UI\Form::FILLED)
                                ->addRule(UI\Form::MAX_FILE_SIZE, 'max-file-size-is*NumberFix*', 5 * 1024 * 1024);
                    }
                }

                // Pokud je pole povinne, nastavi se mu required
                if ($input['required'] == true && $input['type'] !== 'image') {
                    $formInput->addRule(UI\Form::FILLED, 'field-is-mandatory');
                }

                // Pokud je pole nastavene na disabled, tak se disabluje
                if (isset($input['readonly'])) {
                    if ($input['readonly'] == true) {
                        $formInput->setAttribute('readonly');
                    }
                }

                // Pokud existuje vychozi hodnota, tak se vlozi
                if (isset($input['value']) && ($input['type'] !== 'image' || $input['type'] !== 'file')) {
                    if ($input['value'] !== '' || $input['value'] !== null) {
                        if ($input['type'] == 'date') {
                            $formInput->setDefaultValue($input['value']->format('Y-m-d H:i:s'));
                        } else {
                            $formInput->setDefaultValue($input['value']);
                        }
                    }
                }
            }
        }

        $form->addSubmit('save', 'save');
        $form->addSubmit('saveNew', 'save-new');

        $form->onError[] = function ($form) {
            if ($form->hasErrors()) {
                $this->invalidateControl('contentFormErrors');
            }
        };

        $form->onSuccess[] = callback($this, 'submitted');

        return $form;
    }

    public function submitted(UI\Form $form) {
        $values = $form->values;
        foreach ($values as $valueKey => $value) {
            foreach ($this->groupedInputs as &$mainKeys) {
                foreach ($mainKeys as $rowKey => &$inputs) {
                    if (!empty($inputs[$valueKey])) {
			if ($inputs[$valueKey]['type'] == 'bool') {
			    $value = $value == 0 ? false : true;
			}
                        $inputs[$valueKey]['value'] = empty($value) ? $value === false ? false : null : $value;
                    }
                }
            }
        }

        try {
            // Ulozeni vsech hodnot na zaklade modules flow
            $this->saveContentForm($this->models);

            // Flash message
            $this->presenter->flashMessage('changes-were-saved', 'success');

            // Nastaveni session pro ajax
            if ($this->presenter->isAjax()) {
                $this->redrawControl('contentForm');
                $this->presenter->setAjaxReloadSession($this->snippets);
            }

            // Presmerovani
            if ($form->submitted->name == 'saveNew') {
                $moduleID = key($this->redirectID);
                $this->presenter->redirect('this', array($moduleID => null));
            } else {
                if ($this->redirectID) {
                    $this->presenter->redirect('this', $this->redirectID);
                } else {
                    $this->presenter->redirect('this');
                }
            }
        } catch (\SuperFCore\Utils\Exception $ex) {
            $form->addError($ex->getMessage());
            $this->invalidateControl('contentFormErrors');
        }
    }

    public function saveContentForm($modulesFlow, $parentModule = null) {
        foreach ($modulesFlow as $moduleKey => $moduleData) {
            if (array_key_exists($moduleKey, $this->groupedInputs)) {
                if (!$parentModule) {
                    $moduleData['parent_id'] = isset($moduleData['parent_id']) ? $moduleData['parent_id'] : null;
                    $moduleData['parent_id_row'] = isset($moduleData['parent_module']) ? $moduleData['parent_module'] . '_id' : null;
                } elseif (isset($moduleData['parent_module'])) {
                    $moduleData['parent_id'] = isset($moduleData['parent_id']) ? $moduleData['parent_id'] : null;
                    $moduleData['parent_id_row'] = isset($moduleData['parent_module']) ? $moduleData['parent_module'] . '_id' : null;
                } else {
                    $moduleData['parent_id'] = $parentModule['id'];
                    $moduleData['parent_id_row'] = $parentModule['name'] . '_id';
                }

                $lastID = $moduleData['model']->saveFromContentForm($moduleData['parent_id'], $moduleData['parent_id_row'], $this->groupedInputs[$moduleKey], isset($moduleData['settings']) ? $moduleData['settings'] : null);
            }
            if (array_key_exists('redirect', $moduleData)) {
                if ($moduleData['redirect'] == true) {
                    $this->redirectID = array(
                        (empty($moduleData['redirectMask']) ? $moduleKey : $moduleData['redirectMask']) . 'ID' => (isset($lastID) ? ($lastID !== $moduleData['id'] ? $lastID : $moduleData['id']) : $moduleData['id'])
                    );
                }
            }

            if (array_key_exists('children', $moduleData)) {
                $parentData = array(
                    'id' => isset($lastID) ? $lastID : $moduleData['id'],
                    'name' => $moduleKey
                );
                $this->saveContentForm($moduleData['children'], $parentData);
            }
        }
    }

    public function handleDeleteItem($id, $module = 'gallery') {
        $item = $this->presenter->context->getService($module)->findByID($id);
        if ($item) {
            $this->presenter->context->getService($module)->deleteItem($item);
            $this->presenter->flashMessage('item-was-deleted', 'success');
        }
        if ($this->presenter->isAjax()) {
            $this->getForContentForm($this->models);
            $this->redrawControl();
        } else {
            $this->presenter->redirect('this');
        }
    }
    
    public function handleSafeUpload() {
        $request = $this->presenter->request;
        $files = $request->getFiles();
        $fileProcessor = $this->presenter->context->getService('fileProcessor');
        if (!empty($files['imgWsw'])) {
            $image = $files['imgWsw'];
            if ($image->isImage()) {
                $path = '/user_uploads/wsw/images/';
                $fileName = $fileProcessor->saveFile($image, WWW_DIR.$path);
                if (!empty($fileName)) {
                    $this->presenter->payload->link = $path.$fileName;
                    $this->presenter->sendPayload();
                }
            }
        } elseif (!empty($files['fileWsw'])) {
            $file = $files['fileWsw'];
            if ($file->getContentType() == 'application/pdf' || $file->getContentType() == 'application/msword' || $file->getContentType() == 'application/excel') {
                $path = '/user_uploads/wsw/files/';
                $fileName = $fileProcessor->saveFile($file, WWW_DIR.$path);
                if (!empty($fileName)) {
                    $this->presenter->payload->link = $path.$fileName;
                    $this->presenter->sendPayload();
                }
            }
        }
    }
    
    public function handleSafeDelete() {
        $request = $this->presenter->request;
        $type = $request->getPost('type');
        $fileName = $request->getPost('file_name');
        $fileProcessor = $this->presenter->context->getService('fileProcessor');
        if ($type == 'imgWsw') {
            $path = WWW_DIR.'/user_uploads/wsw/images/';
        } elseif ($type == 'fileWsw') {
            $path = WWW_DIR.'/user_uploads/wsw/files/';
        }
        if (!empty($path)) {
            $fileProcessor->deleteFile($path.$fileName);
        }
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/FormContent.latte');
        $this->template->setTranslator($this->presenter->translator);

        // Zobrazovac nahledu obrazku
        $mrHelpers = $this->presenter->context->getService('superFHelpers');
        $this->template->registerHelper('getPreview', array($mrHelpers, 'getPreview'));
        $this->template->registerHelper('getFileUrl', array($mrHelpers, 'getFileUrl'));

        // Vlozeni statickych inputu do sablony
        $this->template->staticInputs = $this->staticInputs;

        $this->template->saveButtons = $this->saveButtons;
        $this->template->render();
    }

}
