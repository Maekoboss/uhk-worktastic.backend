<?php

    namespace AdminModule\CoreModule\GalleryModule;

    use Nette\Application\UI;

    class ComponentGallery extends UI\Control {

	private $model = array();
	private $snippets = array();
	private $staticInputs;
	private $mainKey;
	private $images;

	public function __construct($staticInputs, $model, $snippets = array('content')) {
	    $this->model = $model;
	    $this->staticInputs = $staticInputs;

	    // Seznam snippetu, ktere se refreshnou po dokonceni pozadavku
	    $this->snippets = $snippets;

	    // groupnuti inputu podle modulu a main_key
	    foreach ($this->staticInputs as $inputKey => $data) {
		$this->mainKey = $data['main_key'];
	    }

	    // Loadnuti dat
	    foreach ($this->staticInputs as $input) {
		$this->getImages($this->mainKey, $this->model['parent_id'], $this->model['parent_module']);
		break;
	    }
	}

	public function getImages($mainKey, $parentID, $parentModule) {
	    $parentModuleInfo = array($parentModule . '_id' => $parentID);
	    $this->images = $this->model['model']->getImagesByKey($parentModuleInfo, $mainKey);
	}

	public function createComponentFormUpload() {
	    $form = new UI\Form();
	    $form->setTranslator($this->presenter->translator);

	    foreach ($this->staticInputs as $inputData) {
		$form->addUpload($inputData['main_key'], $inputData['title'], true)
			->addRule(UI\Form::FILLED, 'upload-at-least-one-image')
			->addRule(UI\Form::IMAGE, 'supported-formats-are-jpg-png-gif')
			->addRule(UI\Form::MAX_FILE_SIZE, 'max-file-size-is*NumberFix*', 20 * 1024 * 1024);
	    }

	    $form->addSubmit('upload', 'upload');
	    $form->onError[] = function ($form) {
		if ($form->hasErrors()) {
		    $this->invalidateControl('formUploadErrors');
		}
	    };
	    $form->onSuccess[] = callback($this, 'submitted');
	    return $form;
	}

	public function submitted($form) {
	    $values = $form->values;
	    foreach ($values[$this->mainKey] as $image) {
		$addArray = array(
		    $this->model['parent_module'] . '_id' => $this->model['parent_id'],
		    'main_key' => $this->mainKey,
		    'file_name' => $image
		);
		$this->model['model']->updateItem(null, $addArray);
	    }

	    // Flash a presmerovani
	    $this->presenter->flashMessage('images-were-uploaded', 'success');

	    // Nastaveni session pro ajax
	    if ($this->presenter->isAjax()) {
		$this->presenter->setAjaxReloadSession($this->snippets);
	    }
	    $this->presenter->redirect('this');
	}

	public function createComponentListingGeneral() {
	    $items = array(
		'where' => array(
		    $this->model['parent_module'] . '_id' => $this->model['parent_id'],
		    'main_key' => $this->mainKey
		),
		'order' => 'sort',
		'columns' => array(
		    array(
			'appearance' => 'image',
			'title' => 'Image'
		    ),
		    array(
			'column' => 'title',
			'appearance' => 'text',
			'title' => 'Title'
		    )
		)
	    );
	    $paginator = array(
		'type' => 'endless'
	    );
	    $tools = array(
		'sort' => array(
		    'snippets' => array('content')
		),
		'delete' => array(
		    'snippets' => array('content')
		)
	    );

	    return new \AdminModule\VisualComponents\ListingGeneral($this->model['model'], $items, $paginator, $tools, false, false);
	}

	public function render() {
	    $this->template->setFile(__DIR__ . '/ComponentGallery.latte');
	    $this->template->setTranslator($this->presenter->translator);
	    // Vlozeni statickych inputu do sablony
	    $this->template->mainKey = $this->mainKey;

	    $this->template->images = $this->images->fetchAll();
	    $this->template->render();
	}

    }
    