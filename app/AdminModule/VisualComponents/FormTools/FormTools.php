<?php

    namespace AdminModule\VisualComponents;

    use Nette\Application\UI,
	Nette\ComponentModel\IContainer,
	Nette\Utils\Strings;

    class FormTools extends UI\Control {

	/** @var UI\Presenter */
	private $presenter;
	private $tools;
	private $model;

	/** @var \Nette\Database\Table\Selection */
	private $items;

	/** @var \Nette\Database\Table\ActiveRow */
	private $item;
	private $sort;

	public function __construct($presenter, $tools, $model, $items, $itemID) {
	    parent::__construct();
	    $this->presenter = $presenter;
	    $this->tools = $tools;
	    $this->model = $model;

	    // zakladni data o aktualnim itemu
	    $this->items = $items;
	    $this->item = $items[$itemID];
	    if (empty($this->item)) {
		$this->item = $this->model->findById($itemID);
	    }
	}

	public function createComponentFormTools() {
	    $form = new UI\Form;
	    $form->setTranslator($this->presenter->translator);

	    $form->addHidden('id', $this->item->id);
	    $form->addHidden('tool');
	    foreach ($this->tools as $tool => $settings) {
		if ($tool == 'sort') {
		    // hidden field pro nove poradi
		    $form->addHidden('sort');
		} elseif ($tool == 'delete') {
		    // deletes
		    $form->addSubmit('delete', 'delete');
		} elseif ($tool == 'detache') {
		    // detache
		    $form->addSubmit('detache', 'delete');
		}
	    }

	    $form->onSuccess[] = callback($this, 'submitted');
	    return $form;
	}

	public function submitted($form) {
	    $tool = $form->values->tool;
	    if ($tool == 'sort' && !empty($form->values->sort)) {
		$oldSort = $this->item->sort;
		$sort = $form->values->sort;
		if ($oldSort !== (int) $sort) {
		    $this->model->editSort($this->item->id, $sort);
		    $parentModuleInfo = $this->model->getParentModuleInfo($this->item);
		    $this->model->recalculateSortBelow($this->item->id, $sort, $oldSort, $this->item->main_key, $parentModuleInfo);
		    $this->model->recalculateSortAbove($this->item->id, $sort, $oldSort, $this->item->main_key, $parentModuleInfo);
		}
		$this->presenter->flashMessage('new-order-was-saved', 'success');
	    } elseif ($tool == 'delete') {
		$thisModuleInfo = array(
		    $this->model->getTableName() . '_id' => $this->item->id
		);
		if ($this->model->getTableName() !== 'images') {
		    try {
			$this->presenter->context->getService('gallery')->deleteImages($thisModuleInfo);
		    } catch (\PDOException $e) {
			
		    }
		} elseif ($this->model->getTableName() !== 'files') {
		    try {
			$this->presenter->context->getService('files')->deleteFiles($thisModuleInfo);
		    } catch (\PDOException $e) {
			
		    }
		}

		$this->model->deleteItem($this->item->id);
		$this->presenter->flashMessage('item-was-deleted', 'success');
	    } elseif ($tool == 'detache') {
                $this->model->detacheItem($this->item->id);
		$this->presenter->flashMessage('item-was-deleted', 'success');
            }

	    if ($this->presenter->isAjax()) {
		$this->presenter->setAjaxReloadSession($this->tools[$tool]['snippets']);
	    }
	    $this->redirect('this');
	}

	public function render() {
	    $this->template->setFile(__DIR__ . '/FormTools.latte');
	    $this->template->id = $this->item->id;
	    $this->template->tools = $this->tools;
	    $this->template->render();
	}

    }
    