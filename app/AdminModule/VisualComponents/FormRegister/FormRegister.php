<?php

    namespace AdminModule\VisualComponents;

    use Nette\Application\UI,
        Nette\ComponentModel\IContainer,
        Nette\Security as NS;

    class FormRegister extends UI\Control {

        public function createComponentFormRegister() {
            $form = new UI\Form;
            $form->setTranslator($this->presenter->translator);
            $form->addText('name', 'name')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('email', 'email')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addPassword('password', 'password')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addPassword('passwordAgain', 'password-again')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory')
                    ->addRule(UI\Form::EQUAL, 'passwords-must-match', $form['password']);

            $form->addText('companyName', 'company-name')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('street', 'street')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('city', 'city')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('zip', 'zip')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('country', 'country')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('companyID', 'company-id')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');
            $form->addText('vatID', 'vat-id')
                    ->addRule(UI\Form::FILLED, 'field-is-mandatory');

            $form->addSubmit('send', 'Register');

            $form->getElementPrototype()->attrs['data-ajax'] = 'false';
            $form->onSuccess[] = callback($this, 'submitted');
            return $form;
        }

        public function submitted($form) {
            try {
                $values = $form->getValues();

                $companyAlreadyExists = $this->presenter->database->table('companies')->where('company_id', $values->companyID)->fetch();
                $userAlreadyExists = $this->presenter->database->table('users')->where('email', $values->email)->fetch();
                if ($companyAlreadyExists) {
                    throw new \SuperFCore\Utils\Exception('this-company-id-is-used');
                } elseif ($userAlreadyExists) {
                    throw new \SuperFCore\Utils\Exception('this-user-alredy-exists');
                } else {
                    // Company Data
                    $companyData = array(
                        'name' => $values->companyName,
                        'street' => $values->street,
                        'city' => $values->city,
                        'zip' => $values->zip,
                        'country' => $values->country,
                        'company_id' => $values->companyID,
                        'vat_id' => $values->vatID,
                        'pin' => $this->generatePIN()
                    );
                    $company = $this->presenter->database->table('companies')->insert($companyData);

                    // User Data
                    $userModel = $this->presenter->context->getService('users');
                    $userData = array(
                        'active' => 1,
                        'username' => $values->email,
                        'email' => $values->email,
                        'name' => $values->name,
                        'role' => 'companyAdmin',
                        'password' => $userModel->calculateHash($values->password),
                        'companies_id' => $company->id
                    );
                    $user = $this->presenter->database->table('users')->insert($userData);

                    // Everything should be ok user can be signed in
                    $this->presenter->getUser()->login($values->email, $values->password, null);
                    $this->presenter->redirect('Company:Homepage:');
                }
            } catch (\SuperFCore\Utils\Exception $e) {
                $form->addError($this->presenter->translator->translate($e->getMessage()));
            }
        }

        public function generatePIN() {
            return mt_rand(10000, 99999);
        }

        public function render() {
            $this->template->setFile(__DIR__ . '/FormRegister.latte');
            $this->template->render();
        }

    }
    