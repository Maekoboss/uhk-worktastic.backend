<?php

namespace AdminModule\VisualComponents;

use Nette\Application\UI;

class MenuTabs extends UI\Control {

	private $menu;
	private $translator;
	private $type;

	public function __construct($menu, $translator, $type = 'nav-tabs') {

		parent::__construct();
		$this->menu = $menu;
		$this->translator = $translator;
		$this->type = $type;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/MenuTabs.latte');
		$this->template->menu = $this->menu;
		$this->template->setTranslator($this->translator);
		$this->template->type = $this->type;
		$this->template->render();
	}

}