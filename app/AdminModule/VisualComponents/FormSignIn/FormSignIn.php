<?php

namespace AdminModule\VisualComponents;

use Nette\Application\UI,
    Nette\ComponentModel\IContainer,
    Nette\Security as NS;

class FormSignIn extends UI\Control {

    private $presenter;
    private $translator;

    public function attached($presenter) {
	parent::attached($presenter);
	$this->presenter = $presenter;
	$this->translator = $this->presenter->translator;
    }

    public function createComponentFormSignIn() {
	$form = new UI\Form;
	$form->setTranslator($this->presenter->translator);
	$form->addText('username', 'username')
		->addRule(UI\Form::FILLED, 'field-is-mandatory');
	$form->addPassword('password', 'password')
		->addRule(UI\Form::FILLED, 'field-is-mandatory');
	$form->addCheckbox('remember', 'remember-me');


	$form->addSubmit('send', 'log-in');

	$form->getElementPrototype()->attrs['data-ajax'] = 'false';
	$form->onSuccess[] = callback($this, 'submitted');
	return $form;
    }

    public function submitted($form) {
	try {
	    $values = $form->getValues();
	    if ($values->remember) {
		$this->presenter->getUser()->setExpiration('+14 days', FALSE);
	    } else {
		$this->presenter->getUser()->setExpiration('+30 minutes', TRUE);
	    }
	    // Pokus o prihlaseni
	    $this->presenter->getUser()->login($values->username, $values->password, null);

	    // Zjisti puvodni request a presmeruje na nej 
	    $this->presenter->restoreRequest($this->presenter->backlink);
	    $this->presenter->redirect('Company:Homepage:');
	} catch (NS\AuthenticationException $e) {
	    $form->addError($this->translator->translate($e->getMessage()));
	}
    }

    public function render() {
	$this->template->setFile(__DIR__ . '/FormSignIn.latte');
	$this->template->render();
    }

}
