<?php

namespace AdminModule\VisualComponents;

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */
use Nette\Application\UI\Control;
use Nette\Utils\Paginator;

/**
 * Visual paginator control.
 *
 * @author     David Grudl
 * @copyright  Copyright (c) 2009 David Grudl
 * @package    Nette Extras
 */
class PaginatorEndless extends Control {

    /** @var Paginator */
    private $paginator;

    /** @persistent */
    public $page = 1;
    public $snippets;
    private $continue = false;

    public function __construct($snippets = array('content')) {
	$this->snippets = $snippets;
    }

    /**
     * @return Nette\Paginator
     */
    public function getPaginator() {
	if (!$this->paginator) {
	    $this->paginator = new Paginator;
	    $this->paginator->itemsPerPage = 20;
	}
	return $this->paginator;
    }

    /**
     * Loads state informations.
     * @param  array
     * @return void
     */
    public function loadState(array $params) {
	parent::loadState($params);
	if ($this->presenter->isAjax()) {
	    $controlName = $this->getName();
	    $currentPage = $this->getPaginator()->page;
	    $ajaxSession = $this->presenter->getSession('ajax');
	    if (!empty($ajaxSession->$controlName) && $currentPage !== $this->page) {
		foreach ($this->snippets as $snippet) {
		    $this->presenter->redrawControl($snippet);
		}
	    }
	    $ajaxSession->$controlName = $this->page;
	}
	$this->getPaginator()->page = $this->page;
    }
    
    /**
     * Renders paginator.
     * @return void
     */
    public function render() {
	$this->template->setFile(dirname(__FILE__) . '/PaginatorEndless.latte');
	$this->template->setTranslator($this->presenter->translator);

	$paginator = $this->getPaginator();
	$page = $paginator->page;
	if ($page < $paginator->getLastPage()) {
	    $this->continue = true;
	}
	$this->template->continue = $this->continue;
	$this->template->paginator = $paginator;
	$this->template->render();
    }

}
