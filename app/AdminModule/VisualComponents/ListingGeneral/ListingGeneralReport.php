<?php

    namespace AdminModule\VisualComponents;

    use Nette\Application\UI;

    class ListingGeneralReport extends ListingGeneral {

        protected $users;

        // Load items
        public function loadItems() {
            $where = $this->itemsColumns['where'];
            $order = $this->itemsColumns['order'];
            // Filters
            if (!empty($this->filtersComponent)) {
                $filterParams = $this['formFilter']->params['filterValues'];
                foreach ($this->filtersComponent as $columnName => $data) {
                    if (!empty($filterParams[$columnName])) {
                        switch ($data['type']) {
                            case 'search':
                                $where[$columnName . ' LIKE ?'] = '%' . $filterParams[$columnName] . '%';
                                break;
                            case 'dropdown':
                                $where[$columnName] = $filterParams[$columnName];
                            case 'month':
                                $startDate = \Nette\Utils\DateTime::createFromFormat('Y-m-d', $filterParams[$columnName] . '-01');
                                $stopDate = \Nette\Utils\DateTime::createFromFormat('Y-m-d', $filterParams[$columnName] . '-01');
                                $stopDate->modify('last day of this month');
                                $where[$columnName . ' >= ? AND ' . $columnName . ' <= ?'] = array($startDate->format('Y-m-d'), $stopDate->format('Y-m-d'));
                        }
                    } elseif ($data['type'] == 'month') {
                        $startDate = new \Nette\Utils\DateTime();
                        $startDate->modify('first day of this month');
                        $stopDate = new \Nette\Utils\DateTime();
                        $stopDate->modify('last day of this month');
                        $where[$columnName . ' >= ? AND ' . $columnName . ' <= ?'] = array($startDate->format('Y-m-d'), $stopDate->format('Y-m-d'));
                    }
                }
            }
            // Paginator
            $paginator = null;
            if (!empty($this->paginatorComponent)) {
                $paginator = $this['paginator']->getPaginator();
                $paginator->itemsPerPage = 25;
            }
            // Items
            $users = $this->model->getUsers($where);
            $finalData = new \Nette\Utils\ArrayHash();
            if ($users) {
                foreach ($users as $userID => $user) {
                    $finalData->offsetSet($userID, new \Nette\Utils\ArrayHash());
                    $finalData->$userID->offsetSet('userData', $user);
                    $finalData->$userID->offsetSet('stats', $this->model->getStats($user, $where));
                }
            }
            return $finalData;
        }

    }
    