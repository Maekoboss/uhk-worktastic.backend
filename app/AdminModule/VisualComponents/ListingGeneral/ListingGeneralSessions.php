<?php

    namespace AdminModule\VisualComponents;

    use Nette\Application\UI;

    class ListingGeneralSessions extends ListingGeneral {

        // Load items
        public function loadItems() {
            $where = $this->itemsColumns['where'];
            $order = $this->itemsColumns['order'];
            // Filters
            if (!empty($this->filtersComponent)) {
                $filterParams = $this['formFilter']->params['filterValues'];
                foreach ($this->filtersComponent as $columnName => $data) {
                    if (!empty($filterParams[$columnName])) {
                        switch ($data['type']) {
                            case 'search':
                                $where[$columnName . ' LIKE ?'] = '%' . $filterParams[$columnName] . '%';
                                break;
                            case 'dropdown':
                                $where[$columnName] = $filterParams[$columnName];
                        }
                    }
                }
            }
            // Paginator
            $paginator = null;
            if (!empty($this->paginatorComponent)) {
                $paginator = $this['paginator']->getPaginator();
                $paginator->itemsPerPage = 25;
            }
            // Items List
            $this->items = $this->model->getGeneralListing($paginator, $where, $order)->fetchAll();
            $items = null;
            if ($this->items) {
                $items = new \Nette\Utils\ArrayHash();
                foreach ($this->items as $id => $item) {
                    $items->offsetSet($id, new \Nette\Utils\ArrayHash);
                    $items->$id->offsetSet('data', $item);
                    $items->$id->offsetSet('userName', $item->users->name);
                    $items->$id->offsetSet('start', \Nette\Utils\ArrayHash::from(array('app_start' => $item->app_start, 'start' => $item->start)));
                    $items->$id->offsetSet('stop', \Nette\Utils\ArrayHash::from(array('app_stop' => $item->app_stop, 'stop' => $item->stop)));
                    $startDate = $item->app_start !== null ? $item->app_start : $item->start;
                    $endDate = $item->app_stop !== null ? $item->app_stop : $item->stop;
                    $duration = $endDate->diff($startDate);
                    $items->$id->offsetSet('duration', $duration->format('%H:%I:%S'));
                }
            }
            return $items;
        }

    }
    