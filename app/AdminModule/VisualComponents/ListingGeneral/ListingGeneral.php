<?php

    namespace AdminModule\VisualComponents;

    use Nette\Application\UI;

    class ListingGeneral extends UI\Control {

        protected $model;
        protected $itemsColumns;
        protected $items;
        protected $paginatorComponent;
        protected $toolsComponent;
        protected $buttonsComponent;
        protected $filtersComponent;
        protected $translator;
        protected $templateName;

        public function __construct($model, $itemsColumns, $paginatorComponent = false, $toolsComponent = false, $buttonsComponent = false, $filtersComponent = false, $templateName = null) {
            parent::__construct();
            $this->model = $model;
            $this->itemsColumns = $itemsColumns;
            $this->paginatorComponent = $paginatorComponent;
            $this->toolsComponent = $toolsComponent;
            $this->buttonsComponent = $buttonsComponent;
            $this->filtersComponent = $filtersComponent;
            $this->templateName = $templateName;
        }

        // Load items
        public function loadItems() {
            $where = $this->itemsColumns['where'];
            $order = $this->itemsColumns['order'];
            // Filters
            if (!empty($this->filtersComponent)) {
                $filterParams = $this['formFilter']->params['filterValues'];
                foreach ($this->filtersComponent as $columnName => $data) {
                    if (!empty($filterParams[$columnName])) {
                        switch ($data['type']) {
                            case 'search':
                                $where[$columnName . ' LIKE ?'] = '%' . $filterParams[$columnName] . '%';
                                break;
                            case 'dropdown':
                                $where[$columnName] = $filterParams[$columnName];
                        }
                    }
                }
            }
            // Paginator
            $paginator = null;
            if (!empty($this->paginatorComponent)) {
                $paginator = $this['paginator']->getPaginator();
                $paginator->itemsPerPage = 25;
            }
            // Items List
            $this->items = $this->model->getGeneralListing($paginator, $where, $order)->fetchAll();
            $items = null;
            if ($this->items) {
                $items = new \Nette\Utils\ArrayHash();
                // Prepare columns to set right section
                $editedItemsColumns = $this->itemsColumns['columns'];
                foreach ($this->itemsColumns['columns'] as $key => $columnData) {
                    if (!empty($columnData['relation'])) {
                        $editedItemsColumns[$key]['base'] = $columnData['relation_table'];
                        $editedItemsColumns[$key]['column_name'] = !empty($columnData['relation_column']) ? $columnData['relation_column'] : null;
                    } else {
                        $editedItemsColumns[$key]['base'] = 'data';
                        $editedItemsColumns[$key]['column_name'] = !empty($columnData['column']) ? $columnData['column'] : null;
                    }
                }
                $this->itemsColumns['columns'] = $editedItemsColumns;
                // Prepare data for output
                foreach ($this->items as $id => $itemData) {
                    $items->offsetSet($id, new \Nette\Utils\ArrayHash());
                    $items->$id->offsetSet('data', $itemData);
                    foreach ($this->itemsColumns['columns'] as $key => $columnData) {
                        if (!empty($columnData['relation'])) {
                            if ($columnData['relation'] == 'hasOne') {
                                $items->$id->offsetSet($columnData['relation_table'], $itemData->ref($columnData['relation_table'], $columnData['relation_key']));
                            } else {
                                $items->$id->offsetSet($columnData['relation_table'], $itemData->related($columnData['relation_table'] . '.' . $columnData['relation_key']));
                                if (!empty($columnData['relation_where'])) {
                                    $items->$id->$columnData['relation_table']->where($columnData['relation_where']);
                                }
                                if ($columnData['relation'] == 'hasOneReversed') {
                                    $items->$id->offsetSet($columnData['relation_table'], $items->$id->$columnData['relation_table']->fetch());
                                } else {
                                    $items->$id->offsetSet($columnData['relation_table'], $items->$id->$columnData['relation_table']->fetchAll());
                                }
                            }
                        }
                    }
                }
            }
            return $items;
        }

        // Paginator
        public function createComponentPaginator() {
            if ($this->paginatorComponent) {
                if ($this->paginatorComponent['type'] == 'endless') {
                    return new \AdminModule\VisualComponents\PaginatorEndless();
                }
            }
        }

        // Tools
        public function createComponentFormTools() {
            if ($this->toolsComponent) {
                $presenter = $this->presenter;
                $items = $this->items;
                $mainModel = $this->model;
                $tools = $this->toolsComponent;
                return new \Nette\Application\UI\Multiplier(function ($itemID) use($presenter, $tools, $mainModel, $items) {
                    return new \AdminModule\VisualComponents\FormTools($presenter, $tools, $mainModel, $items, $itemID);
                });
            }
        }

        // Filters
        public function createComponentFormFilter() {
            if ($this->filtersComponent) {
                return new \AdminModule\VisualComponents\FormFilter($this->filtersComponent);
            }
        }

        public function render() {
            if ($this->templateName) {
                $this->template->setFile(__DIR__ . '/ListingGeneral' . $this->templateName . '.latte');
            } else {
                $this->template->setFile(__DIR__ . '/ListingGeneral.latte');
            }
            $this->template->setTranslator($this->presenter->translator);
            $now = new \Nette\Utils\DateTime;
            $this->template->now = $now->format('Y-m-d H:i:s');

            // Helpers
            $mrHelpers = $this->presenter->context->getService('superFHelpers');
            $this->template->registerHelper('datesInWords', array($mrHelpers, 'datesInWords'));
            $this->template->registerHelper('getPreview', array($mrHelpers, 'getPreview'));

            // Items
            $this->template->items = $this->loadItems();
            $this->template->itemsColumns = $this->itemsColumns;

            // Buttons
            $this->template->buttons = $this->buttonsComponent;
            // Paginator
            $this->template->paginatorComponent = $this->paginatorComponent;
            // Tools Form
            $this->template->toolsComponent = $this->toolsComponent;
            // Filters Form
            $this->template->filtersComponent = $this->filtersComponent;

            $this->template->render();
        }

    }
    