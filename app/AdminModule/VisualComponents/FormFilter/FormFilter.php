<?php

namespace AdminModule\VisualComponents;

use Nette\Application\UI;

class FormFilter extends UI\Control {

    private $snippets;
    /** @persistent array */
    public $filterValues = array();
    private $filters;

    public function __construct($filters, $snippets = array('content')) {
	parent::__construct();
	$this->snippets = $snippets;
	$this->filters = $filters;
    }

    public function createComponentFormFilter() {
	$form = new UI\Form;
	$form->setTranslator($this->presenter->translator);
	$form->setMethod('get');
	$filterValues = $form->addContainer('filterValues');
	foreach ($this->filters as $filterKey => $filter) {
	    if ($filter['type'] == 'search') {
		$filterValues->addText($filterKey, '', $filter['size'])
		    ->setAttribute('placeholder', $filter['placeholder']);
	    } elseif ($filter['type'] == 'dropdown') {
		$filterValues->addSelect($filterKey, null, $filter['options']);
		if (!empty($filter['prompt'])) {
		    $filterValues[$filterKey]->setPrompt($filter['prompt']);
		}
            } elseif ($filter['type'] == 'month') {
                $filterValues->addText($filterKey, null);
            }

	    if (isset($this->filterValues[$filterKey])) {
		$filterValues[$filterKey]->setDefaultValue($this->filterValues[$filterKey]);
	    } elseif ($filter['type'] == 'month') {
                $defaultDate = new \Nette\Utils\DateTime();
                $filterValues[$filterKey]->setDefaultValue($defaultDate->format('Y-m'));
            }
	}
	$form->addSubmit('filter', 'filter');
	$form->onSuccess[] = callback($this, 'submitted');
	return $form;
    }

    public function submitted($form) {
	$values = $form->getValues(true);
	if ($this->presenter->isAjax()) {
	    $this->presenter->setAjaxReloadSession($this->snippets);
	}
	$this->redirect('this', $values);
    }

    public function render() {
	$this->template->setFile(__DIR__ . '/FormFilter.latte');
	$this->template->filters = $this->filters;
	$this->template->render();
    }

}
