<?php

namespace AdminModule\VisualComponents;

use Nette\Application\UI;

class MenuSidebar extends UI\Control {

	private $menu;
	private $translator;

	public function __construct($menu, $translator) {
		parent::__construct();
		$this->menu = $menu;
		$this->translator = $translator;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/MenuSidebar.latte');
		$this->template->menu = $this->menu;
		$this->template->setTranslator($this->translator);
		$this->template->render();
	}

}