<?php

namespace AdminModule\Models;

use \Nette\Utils\Finder;

class Gallery extends General\Table {

    /** @var FileProcessor */
    protected $fileProcessor;
    protected $path = '/user_uploads/gallery/';

    public function __construct(\Nette\Database\Context $database, $tableName, FileProcessor $fileProcessor) {
        $this->database = $database;
        $this->tableName = $tableName;
        $this->fileProcessor = $fileProcessor;
    }

    //-------------------------------------- GETTERY ------------------------------------------------

    /**
     * Returns images by parent and main_key
     * @param  Array	List of main_keys
     * @param  \Nette\Database\Table\ActiveRow || array Parent row data or array foreign_key_id => id
     * @return \Nette\Database\Table\Selection  Object containing set of rows for given parent
     */
    public function getImagesByKey($parentModuleInfo, $mainKeys, $sort = 'sort') {
        if (is_array($parentModuleInfo)) {
            $images = $this->database->table($this->getTableName())->where('main_key', $mainKeys)
                            ->where($parentModuleInfo)
                            ->order('main_key')->order($sort);
        } else {
            $images = $parentModuleInfo->related($this->getTableName())->where('main_key', $mainKeys)
                            ->order('main_key')->order($sort);
        }
        return $images;
    }

    //-------------------------------------- CRUD METODY --------------------------------------------  
    public function updateItem($item, $values) {
        if ($this->settings['lang'] == true && !array_key_exists('lang', $values)) {
            $values['lang'] = $this->lang;
        }
        if ($this->settings['sort'] == true && !array_key_exists('sort', $values)) {
            $values['sort'] = '1';
        }

        if (!empty($values['file_name']->name)) {
            if (!empty($item)) {
                $parentModuleInfo = $this->getParentModuleInfo($item);
                $values[(key($parentModuleInfo))] = array_values($parentModuleInfo)[0];
                $values['main_key'] = $item->main_key;
                $this->deleteItemOnlyFile($item);
            } else {
                $parentModuleInfo = $this->getParentModuleInfo($values);
            }

            // File path and image creation
            $path = WWW_DIR . $this->path . (key($parentModuleInfo)) . '_' . array_values($parentModuleInfo)[0];
            if (is_dir($path) == false) {
                $this->createGalleryFolders($path);
            }

            $this->fileProcessor->generateOriginalImage($values['file_name'], $path);
            $values['file_name'] = $this->fileProcessor->getFileName();
            $values['width'] = $this->fileProcessor->imageWidth;
            $values['height'] = $this->fileProcessor->imageHeight;
            $values['date'] = new \DateTime;
        } else {
            unset($values['file_name']);
        }
        
        // Insert
        if ($item == null && !empty($values['file_name'])) {
            $item = $this->database->table($this->getTableName())->insert($values);
            if (!empty($values['sort'])) {
                $parentModuleInfo = $this->getParentModuleInfo($item);
                $sortI = $this->getLastSort($values['main_key'], $parentModuleInfo) + 1;
                $this->recalculateSortBelow($item->id, 1, $sortI, $values['main_key'], $parentModuleInfo);
            }
            // Update
        } elseif ($item != null) {
            if (is_int($item)) {
                $item = $this->findById($item);
            }
            $item->update($values);
        }

        return $item != null ? $item->id : null;
    }

     /**
     * Deletes item (files and DB)
     */
    public function deleteItem($image) {
        if (is_numeric($image)) {
            $image = $this->findByID($image);
        }

        $parentModuleInfo = $this->getParentModuleInfo($image);
        //File deletation
        $path = WWW_DIR . $this->path . (key($parentModuleInfo)) . '_' . array_values($parentModuleInfo)[0];
        foreach (Finder::findFiles(array('*w_*h_' . $image->file_name, $image->file_name))->from($path . '/') as $key => $file) {
            unlink($key);
        }
        //DB deletation
        $this->recalculateDeletedSort($image->sort, $image->main_key, $parentModuleInfo);
        $image->delete();
    }

     /**
     * Deletes just files
     */
    public function deleteItemOnlyFile($image) {
        if (is_numeric($image)) {
            $image = $this->findByID($image);
        }

        $parentModuleInfo = $this->getParentModuleInfo($image);
        // File deletation
        $path = WWW_DIR . $this->path . (key($parentModuleInfo)) . '_' . array_values($parentModuleInfo)[0];
        foreach (Finder::findFiles(array('*w_*h_' . $image->file_name, $image->file_name))->from($path . '/') as $key => $file) {
            unlink($key);
        }
    }

    /**
     * Deletes whole gallery from parent row
     */
    public function deleteImages($parentModuleInfo) {
        $path = WWW_DIR . $this->path . (key($parentModuleInfo)) . '_' . array_values($parentModuleInfo)[0];
        $this->deleteFolders($path);

        $this->database->table($this->getTableName())->where($parentModuleInfo)->delete();
    }

    //---------------------------------- HELPER METHODS --------------------------------------------------------

    public function setInputValue($input, $itemData) {
        if ($input['type'] == 'image') {
            $input['value'] = $itemData;
        } else {
            $input['value'] = $itemData->$input['column'];
        }
        return $input;
    }

    /**
     * Vytvori potrebne slozky pro galerii
     * @param  string	cesta k hlavnimu adresari galerie
     */
    public function createGalleryFolders($path) {
        $this->fileProcessor->createFolder($path);
        $this->fileProcessor->createFolder($path . '/forWeb');
        $this->fileProcessor->createFolder($path . '/original');
    }

    /**
     * Smaze slozky dane galerie i s obsahem
     * @param  string	cesta k hlavnimu adresari galerie
     */
    public function deleteFolders($path) {
        $this->fileProcessor->emptyFolder($path . '/forWeb', true);
        $this->fileProcessor->emptyFolder($path . '/original', true);
        $this->fileProcessor->emptyFolder($path, true);
    }

}
