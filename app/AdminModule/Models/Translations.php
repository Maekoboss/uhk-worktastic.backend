<?php

    namespace AdminModule\Models;
    
    use Nette\Caching\Cache;

    class Translations extends General\Table {

	protected $settings = array(
	    'main_key' => false,
	    'lang' => true,
	    'sort' => false,
	    'slug' => false
	);
	
	private $cache;
	
	public function __construct(\Nette\Database\Context $database, $tableName, \Nette\Caching\Storages\FileStorage $storage) {
	    parent::__construct($database, $tableName);
	    $this->cache = new Cache($storage, 'translator');
	}

	public function setUpdateValue($updateArray, $inputData) {
	    switch ($inputData['column']) {
		case 'key':
		    if ($this->tableItems) {
			foreach ($this->tableItems as $item) {
			    foreach ($item->related('translations.parent_id') as $relatedItem) {
				$relatedItem->update(array('key' => $inputData['value']));
			    }
			}
		    }
		    $updateArray[$inputData['column']] = $inputData['value'];
		    break;
		default:
		    $updateArray[$inputData['column']] = $inputData['value'];
		    break;
	    }
	    return $updateArray;
	}
	
	public function updateItem($item, $values) {
	    $this->cache->clean(array(Cache::TAGS => array('translations')));
	    return parent::updateItem($item, $values);
	}

    }
    