<?php

namespace AdminModule\Models;

class ACLsetter extends \Nette\Security\Permission{
    
    public function __construct() {
        
        // Roles
	$this->addRole('guest');
        $this->addRole('registered', 'guest');
        $this->addRole('companyAdmin', 'registered');
	$this->addRole('superAdministrator', 'companyAdmin');
	
	
	// ################ Backend Module ################
	$this->addResource('Admin:');
	$this->addResource('backend.translations.backend');
        
        $this->allow('companyAdmin', 'Admin:', \Nette\Security\Permission::ALL);
	$this->allow('superAdministrator', 'backend.translations.backend', \Nette\Security\Permission::ALL);
    }
    
}
