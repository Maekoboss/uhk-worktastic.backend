<?php

namespace AdminModule\Models;

interface IUniForm {
    
    /**
     * Return rows for given content form
     * @return  array All inputs
     */
    public function getForContentForm($parentID, $parentModuleRow, $inputs, $id = null);
    
    /**
     * Updates the content from content form
     * @return  int ID of inserted or updated row
     */
    public function saveFromContentForm($parentID, $parentModuleRow, $inputs, $settings = null);
    
}
