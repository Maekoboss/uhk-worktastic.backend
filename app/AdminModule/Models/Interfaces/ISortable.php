<?php

namespace AdminModule\Models;

interface ISortable {
    
    public function recalculateSortBelow($id, $sort, $oldSort, $mainKey, $parentInfo = null);
    public function recalculateSortAbove($id, $sort, $oldSort, $mainKey, $parentInfo = null);
    public function recalculateDeletedSort($deletedSort, $mainKey, $parentInfo = null);
    public function getLastSort($mainKey, $parentInfo = null);
    public function editSort($itemID, $sort);    
    
}
