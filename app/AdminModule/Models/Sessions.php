<?php

    namespace AdminModule\Models;

    use \Nette\Utils\Finder;

    class Sessions extends General\Table {

        protected $settings = array(
            'main_key' => false,
            'lang' => false,
            'sort' => false,
            'slug' => false
        );
        
        const SECOND = 1;
        const MINUTE = 60 * self::SECOND;
        const HOUR = 60 * self::MINUTE;
        
        public function getUsers($where) {
            $sessions = $this->database->table($this->getTableName())->where($where)->group('users_id')->fetchAll();
            $userIds = array();
            if ($sessions) {
                foreach ($sessions as $session) {
                    $userIds[] = $session->users_id;
                }
                return $this->database->table('users')->where('id', $userIds);
            } else {
                return null;
            }
        }
        
        public function getStats($user, $where) {
            $returnData = new \Nette\Utils\ArrayHash();
            $sessions = $user->related($this->getTableName())->where($where)->fetchAll();
            $overAllTime = 0;
            $activityTime = array();
            if ($sessions) {
                foreach ($sessions as $session) {
                    // Overall
                    $sessionStart = $session->app_start;
                    $sessionStop = $session->app_stop;
                    $overAllTime = $overAllTime + ($sessionStop->getTimestamp() - $sessionStart->getTimestamp());

                    // Activity statistics
                    $activityData = $session->related('sessions_activities')->order('start DESC')->fetchAll();
                    $infoTableName = 'activities';
                    if ($activityData) {
                        $previousStart = $sessionStop;
                        foreach ($activityData as $detail) {
                            $activityTime[$detail->$infoTableName->name] = (!empty($activityTime[$detail->$infoTableName->name]) ? $activityTime[$detail->$infoTableName->name] : 0) + ($previousStart->getTimestamp() - $detail->start->getTimestamp());
                            $previousStart = $detail->start;
                        }
                        arsort($activityTime);
                    }
                }
                $returnData->offsetSet('duration', $this->convertItervalToString($overAllTime));
                foreach ($activityTime as &$time) {
                    $time = $this->convertItervalToString($time);
                }
                $returnData->offsetSet('activitiesDuration', $activityTime);
                return $returnData;
            } else {
                return null;
            }
            
        }
        
        public function convertItervalToString($interval) {
            return sprintf('%03d',$interval / self::HOUR) . ':' . sprintf('%02d', $interval / self::MINUTE % 60) . ':' . sprintf('%02d', $interval / self::SECOND % 60);
        }

    }
    