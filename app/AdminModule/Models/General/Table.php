<?php

    namespace AdminModule\Models\General;

    class Table extends \Nette\Object implements \AdminModule\Models\IUniForm, \AdminModule\Models\ISortable {

        /** @var \Nette\Database\Context */
        protected $database;
        protected $tableName;
        protected $tableItems = null;
        protected $settings = array(
            'main_key' => true,
            'lang' => true,
            'sort' => true,
            'slug' => false
        );
        protected $lang = 'cz';
        protected $backLang = 'cz';

        public function __construct(\Nette\Database\Context $database, $tableName) {
            $this->database = $database;
            $this->tableName = $tableName;
        }

        /**
         * Returns table name
         * @return  string	module name
         */
        public function getTableName() {
            return $this->tableName;
        }

        /**
         * Set-ups langs
         */
        public function setLang($lang = 'cz', $backLang = 'cz') {
            $this->lang = $lang;
            $this->backLang = $backLang;
        }

        //-------------------------------------- GETTERS ------------------------------------------------
        /**
         * Returns row or NULL
         * @param  int  Item ID
         * @return \Nette\Database\Table\ActiveRow || null  Object containing row from table
         */
        public function findById($id) {
            return $this->database->table($this->getTableName())->get($id);
        }

        /**
         * Return rows for a given parent
         * @param  \Nette\Database\Table\ActiveRow || array Parent row data or array foreign_key_id => id
         * @return \Nette\Database\Table\Selection  Object containing set of rows for given parent
         */
        public function findByParent($parentData, $mainKeys = array()) {
            if (is_array($parentData)) {
                if (empty($mainKeys)) {
                    return $this->database->table($this->getTableName())->where($parentData);
                } else {
                    return $this->database->table($this->getTableName())->where($parentData)->where('main_key', $mainKeys);
                }
            } else {
                if (empty($mainKeys)) {
                    return $parentData->related($this->getTableName());
                } else {
                    return $parentData->related($this->getTableName())->where('main_key', $mainKeys);
                }
            }
        }

        /**
         * Returns rows or NULL
         * @param  array  List of main keys
         * @return \Nette\Database\Table\Selction || false  Object containing rows from table
         */
        public function findByMainKey($mainKeys) {
            return $this->database->table($this->getTableName())->where('lang', $this->lang)->where('main_key', $mainKeys);
        }

        //-------------------------------------- LISTINGS ------------------------------------------------  
        /**
         * Return rows for listing
         * @return  \Nette\Database\Table\Selection Object containing set of rows
         */
        public function getGeneralListing($paginator = null, $where = array(), $order = 'sort') {
            if (!$paginator) {
                return $this->database->table($this->getTableName())
                                ->where($where)
                                ->order(!empty($order) ? $order : 'id');
            } else {
                $paginator->itemCount = $this->database->table($this->getTableName())->where($where)->count();
                $page = $paginator->getPage();
                $length = $page * $paginator->itemsPerPage;
                return $this->database->table($this->getTableName())
                                ->where($where)
                                ->order(!empty($order) ? $order : 'id')
                                ->limit($length);
            }
        }

        //---------------------------------- CONTENT FORM METHODS ----------------------------------------
        public function getForContentForm($parentID, $parentModuleRow, $inputs, $id = null) {
            if (!empty($id)) {
                // For only one possible item in form
                $item = $this->findById($id);
                
                $this->tableItems = null;
                if (!empty($item)) {
                    $this->tableItems[] = $item;
                }
                if (!empty($this->tableItems)) {
                    foreach ($inputs as $mainKey => &$inputsData) {
                        foreach ($inputsData as &$input) {
                            if ($this->tableItems[0]->offsetExists($input['column'])) {
                                $input = $this->setInputValue($input, $this->tableItems[0]);
                            }
                        }
                    }
                }
            } elseif ($parentModuleRow != null && $parentID != null) {
                // For multiple items in form
                $parentModuleInfo[$parentModuleRow] = $parentID;
                $mainKeys = $this->getMainKeys($inputs);

                $this->tableItems = $this->findByParent($parentModuleInfo, $mainKeys)->fetchAll();
                foreach ($inputs as $mainKey => &$inputsData) {
                    foreach ($inputsData as &$input) {
                        if ($this->tableItems) {
                            foreach ($this->tableItems as $itemData) {
                                if ($itemData->main_key == $input['main_key']) {
                                    $input = $this->setInputValue($input, $itemData);
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                // Special for???
                $mainKeys = $this->getMainKeys($inputs);
                $this->tableItems = $this->findByMainKey($mainKeys)->fetchAll();
                foreach ($inputs as $mainKey => &$inputsData) {
                    foreach ($inputsData as &$input) {
                        if ($this->tableItems) {
                            foreach ($this->tableItems as $itemData) {
                                if ($itemData->main_key == $input['main_key']) {
                                    $input = $this->setInputValue($input, $itemData);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return $inputs;
        }

        public function saveFromContentForm($parentID, $parentModuleRow, $inputs, $settings = null) {
            $lastItemID = null;
            if (!empty($settings)) {
                foreach ($settings as $key => $value) {
                    $this->settings[$key] = $value;
                }
            }

            // Updates item
            $updateArray = array();
            if ($this->tableItems) {
                foreach ($this->tableItems as $key => $tableItem) {
                    if ($this->settings['main_key'] == true) {
                        if (array_key_exists($tableItem->main_key, $inputs)) {
                            foreach ($inputs[$tableItem->main_key] as $input) {
                                $updateArray = $this->setUpdateValue($updateArray, $input);
                            }
                            $lastItemID = $this->updateItem($tableItem, $updateArray);
                            $updateArray = array();
                            unset($inputs[$tableItem->main_key]);
                        }
                    } else {
                        foreach ($inputs as $mainKey => $inputGroup) {
                            foreach ($inputGroup as $input) {
                                $updateArray = $this->setUpdateValue($updateArray, $input);
                            }
                            $lastItemID = $this->updateItem($tableItem, $updateArray);
                            $updateArray = array();
                            unset($inputs[$mainKey]);
                        }
                    }
                }
            }

            // Creates new item
            $insertArray = array();
            if ($inputs) {
                foreach ($inputs as $mainKey => $inputs) {
                    // data from inputs
                    foreach ($inputs as $input) {
                        $insertArray = $this->setUpdateValue($insertArray, $input);
                    }
                    // other (static)
                    if ($this->settings['main_key'] == true) {
                        $insertArray['main_key'] = $mainKey;
                    }

                    if ($parentID !== null && $parentModuleRow !== null) {
                        $insertArray[$parentModuleRow] = $parentID;
                    }
                    $lastItemID = $this->updateItem(null, $insertArray);
                    $insertArray = array();
                }
                return $lastItemID;
            }
        }

        //-------------------------------------- CRUD METHODS --------------------------------------------
        /**
         * Saves item data to database
         * @param \Nette\Database\Table\ActiveRow || int || null	Row object, row id or null for create
         * @param  array	Array with values to save
         * @return  int	id of the post
         */
        public function updateItem($item, $values) {
            if ($this->settings['lang'] == true && !array_key_exists('lang', $values)) {
                $values['lang'] = $this->lang;
            }

            // Insert
            if ($item == null) {
                if ($this->settings['sort'] == true && !array_key_exists('sort', $values)) {
                    $values['sort'] = '1';
                }
                $item = $this->database->table($this->getTableName())->insert($values);
                if (!empty($values['sort'])) {
                    $parentModuleInfo = $this->getParentModuleInfo($item);
                    $sortI = $this->getLastSort($values['main_key'], $parentModuleInfo) + 1;
                    $this->recalculateSortBelow($item->id, 1, $sortI, $values['main_key'], $parentModuleInfo);
                }
            } else {
                // Update
                if (is_int($item)) {
                    $item = $this->findById($item);
                }
                $item->update($values);
            }
            // Generate slug if required from settings
            if ($this->settings['slug'] == true) {
                $slug = empty($item->title) ? null : \Nette\Utils\Strings::webalize($item->title) . '_' . $item->id;
                $item->update(array('slug' => $slug));
            }

            return $item->id;
        }

        /**
         * Deletes item from DB
         * @param  \Nette\Database\Table\ActiveRow || int	Database row or ID
         */
        public function deleteItem($item) {
            if (is_int($item)) {
                $item = $this->findById($item);
            }
            $parentModuleInfo = $this->getParentModuleInfo($item);
            if ($this->settings['sort'] == true) {
                $this->recalculateDeletedSort($item->sort, $item->main_key, $parentModuleInfo);
            }
            return $item->delete();
        }

        //---------------------------------- HELPER METHODS --------------------------------------------------------

        /**
         * Returns parent info
         * @param  \Nette\Database\Table\ActiveRow || array Database row or array where to look for parent module
         * @return array Array parent_column => parent_id
         */
        public function getParentModuleInfo($item) {
            $parentModuleRow = array();
            if (!empty($item['structure_id'])) {
                $parentModuleRow['structure_id'] = $item['structure_id'];
            } elseif (!empty($item['posts_id'])) {
                $parentModuleRow['posts_id'] = $item['posts_id'];
            } elseif (!empty($item['companies_id'])) {
                $parentModuleRow['companies_id'] = $item['companies_id'];
            }
            return $parentModuleRow;
        }

        /**
         * Returns list of requested main_keys
         * @param array List of requested input fields
         * @return array List of requested main_keys
         */
        public function getMainKeys($inputs) {
            $mainKeys = array();
            foreach ($inputs as $inputKey => $data) {
                $mainKeys[] = $inputKey;
            }
            return $mainKeys;
        }

        /**
         * Updates input item with correct value data
         * @param array Item data array
         * @param \Nette\Database\Table\ActiveRow Row that contains requested data
         * @return array Updated item data array
         */
        public function setInputValue($input, $itemData) {
            $input['value'] = $itemData->$input['column'];
            return $input;
        }

        /**
         * Prepares value to be updated to the given row
         * @param array Current update array
         * @param array Input data
         * @return array Updated array
         */
        public function setUpdateValue($updateArray, $inputData) {
            $updateArray[$inputData['column']] = $inputData['value'];
            return $updateArray;
        }

        /**
         * Recalculates sort below the given post (for inserting on 1st place or moving up)
         * @param  int | null	post id if null its deleted item and all posts below should move up
         * @param  int	new position
         * @param  int	old position
         * @param  string	main_key
         * @param  array	parent data array(parent_column => parent_id);
         */
        public function recalculateSortBelow($id, $sort, $oldSort, $mainKey, $parentInfo = null) {
            $itemsBelowSort = $this->database->table($this->getTableName())
                            ->where(array('main_key' => $mainKey))
                            ->where($parentInfo)
                            ->where('id <> ' . (int) $id . '')->where('sort >= ?', $sort)->where('sort < ' . (int) $oldSort . '')->where('lang', $this->lang);

            if ($itemsBelowSort->count() > 0) {
                foreach ($itemsBelowSort as $item) {
                    $newSort = $item->sort + 1;
                    $item->update(array(
                        'sort' => $newSort
                    ));
                }
            }
        }

        /**
         * Recalculates sort above the given post
         * @param  int | null	post id if null its deleted item and all posts above should move up
         * @param  int	new position
         * @param  int	old position
         * @param  string	main_key
         * @param  array	parent data array(parent_column => parent_id);
         */
        public function recalculateSortAbove($id, $sort, $oldSort, $mainKey, $parentInfo = null) {
            $itemsAboveSort = $this->database->table($this->getTableName())->where('id <> ' . (int) $id . '')->where(array(
                                'main_key' => $mainKey,
                                'sort <= ?' => $sort))
                            ->where($parentInfo)
                            ->where('sort > ' . (int) $oldSort . '')->where('lang', $this->lang);

            if ($itemsAboveSort->count() > 0) {
                foreach ($itemsAboveSort as $item) {
                    $newSort = $item->sort - 1;
                    $item->update(array(
                        'sort' => $newSort
                    ));
                }
            }
        }

        /**
         * Recalculates sort when item is deleted
         * @param  int	original position of deleted item
         * @param  string	main_key
         * @param  array	parent data array(parent_column => parent_id);
         */
        public function recalculateDeletedSort($deletedSort, $mainKey, $parentInfo = null) {
            $itemsBelowDeletedSort = $this->database->table($this->getTableName())->where(array(
                        'sort >= ?' => $deletedSort,
                        'main_key' => $mainKey,
                        'lang' => $this->lang))
                    ->where($parentInfo);

            if ($itemsBelowDeletedSort->count() > 0) {
                foreach ($itemsBelowDeletedSort as $item) {
                    $newSort = $item->sort - 1;
                    $item->update(array(
                        'sort' => $newSort
                    ));
                }
            }
        }

        /**
         * Returns the sort of last post
         * @param  string	main_key
         * @param  array	parent data array(parent_column => parent_id);
         */
        public function getLastSort($mainKey, $parentInfo = null) {
            $lastSort = $this->database->table($this->getTableName())->select('sort')->where(array(
                        'main_key' => $mainKey,
                        'lang' => $this->lang))
                    ->where($parentInfo);
            if ($lastSort->count() > 0) {
                return $lastSort->max('sort');
            } else {
                return 0;
            }
        }

        /**
         * Updates sort for given post
         * @param  int	post id
         * @param  int	new position
         */
        public function editSort($itemID, $sort) {
            $this->database->table($this->getTableName())->where('id', $itemID)->update(array('sort' => $sort));
        }

    }
    