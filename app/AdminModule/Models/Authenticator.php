<?php

namespace AdminModule\Models;

use \Nette\Security as NS,
    \Nette\Object;

class Authenticator extends Object implements NS\IAuthenticator {

    /** @var \Nette\Database\Context */
    private $database;
    /** @var	Users */
    private $usersModel;
    
    public function __construct(\Nette\Database\Context $database, Users $usersModel) {
	$this->database = $database;
	$this->usersModel = $usersModel;
    }

    public function authenticate(array $credentials) {
	list($username, $password, $userData) = $credentials;
	if (empty($userData)) {
	    $userData = $this->database->table('users')->where('username', $username)->fetch();
	    if (!$userData) {
		throw new NS\AuthenticationException('wrong-username-or-password', self::IDENTITY_NOT_FOUND);
	    }
	    if ($userData->password !== $this->calculateHash($password)) {
		throw new NS\AuthenticationException('wrong-username-or-password', self::INVALID_CREDENTIAL);
	    }
	    if ($userData->active !== 1) {
		throw new NS\AuthenticationException('your-account-was-deactivated', self::NOT_APPROVED);
	    }
	}

	return $this->usersModel->setBasicIdentity($userData->id);
    }

    public function calculateHash($password) {
	return hash('sha512', $password);
    }

}
