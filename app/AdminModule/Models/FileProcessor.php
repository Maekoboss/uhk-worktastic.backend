<?php

    namespace AdminModule\Models;

    use \Nette\Object,
        \Nette\Image,
        \Nette\Utils\Finder;

    class FileProcessor extends Object {

        protected $image;
        protected $file;
        protected $filename;
        protected $path;
        public $imageWidth;
        public $imageHeight;

        //-------------------------------------- Obecne funkce modulu --------------------------------------------

        /**
         * Vrati nazev vytvoreneho souboru
         * @return string	nazev souboru
         */
        public function getFileName() {
            return $this->filename;
        }

        /**
         * Vytvori finalni soubor a vlozi jej na misto
         * @param  file	objekt se souborem
         * @param  string	cesta ke slozce souboru
         */
        public function saveFile($file, $path) {
            $this->file = $file;
            $this->filename = $this->file->getSanitizedName();
            $matchedFileNames = array();
            foreach (Finder::findFiles($this->filename, '*_' . $this->filename)->in($path) as $fileValue) {
                $matchedFileNames[] = $fileValue->getBasename();
            }
            $this->filename = $this->findUniqueName(0, $matchedFileNames, $this->filename);
            $this->path = $path;
            $this->file->move("$this->path/$this->filename");
            return $this->filename;
        }

        public function findUniqueName($i, $array, $fileName) {
            $testString = $i > 0 ? $i . '_' . $fileName : $fileName;
            while (in_array($testString, $array)) {
                $i++;
                $testString = $i . '_' . $fileName;
            }
            return $testString;
        }

        /**
         * Vytvori z obrazku thumby a vlozi je do slozek v galerii
         * @param  image	objekt s obrazkem
         * @param  string	cesta ke galerii
         */
        public function generateOriginalImage($image, $path) {
            $this->image = $image;
            $this->path = $path;
            $this->saveFile($this->image, "$this->path/original/");
            $this->image = $this->image->toImage();

            $this->imageWidth = $this->image->width;
            $this->imageHeight = $this->image->height;
        }

        /**
         * Vytvori z obrazku thumby
         * @param  image	objekt s obrazkem
         * @param  int	sirka nahledu
         * @param  int	vyska nahledu
         * @return  \Nette\Image	vraci nette objekt s obrazkem
         */
        public function createThumb($image, $width, $height, $crop, $greyScale = false, $sharpen = true) {

            if ($width == null || $height == null) {
                $image->resize($width, $height);
                if ($sharpen == true) {
                    $image->sharpen($width, $height);
                }
                return $image;
            } else {
                $width_scale = $image->width / $width;
                $height_scale = $image->height / $height;
                if ($width_scale <= $height_scale && $crop == true) {
                    $image->resize($width, null);
                    $image->crop(0, (int) ($image->height - $height) / 2, $width, $height);
                    if ($sharpen == true) {
                        $image->sharpen($width, $height);
                    }
                    if ($greyScale == true) {
                        $image->filter(IMG_FILTER_GRAYSCALE);
                    }
                    return $image;
                } elseif ($width_scale <= $height_scale && $crop == false) {
                    $image->resize(null, $height);
                    if ($sharpen == true) {
                        $image->sharpen($width, $height);
                    }
                    if ($greyScale == true) {
                        $image->filter(IMG_FILTER_GRAYSCALE);
                    }
                    return $image;
                } elseif ($width_scale > $height_scale && $crop == true) {
                    $image->resize(null, $height);
                    $image->crop((int) ($image->width - $width) / 2, 0, $width, $height);
                    if ($sharpen == true) {
                        $image->sharpen($width, $height);
                    }
                    if ($greyScale == true) {
                        $image->filter(IMG_FILTER_GRAYSCALE);
                    }
                    return $image;
                } elseif ($width_scale > $height_scale && $crop == false) {
                    $image->resize($width, null);
                    if ($sharpen == true) {
                        $image->sharpen($width, $height);
                    }
                    if ($greyScale == true) {
                        $image->filter(IMG_FILTER_GRAYSCALE);
                    }
                    return $image;
                }
            }
        }

        /**
         * Pokusi se najit a vratit thumb v požadované velikosti, pokud takový thumb neexistuje, tak ho vytvoří.
         * @param  \Nette\Database\Table\ActiveRow	objekt obrazku
         * @param  string	uplna cesta do adreare s obrazkem
         * @param  array	Nastaveni thumbu - width, height, crop, greyScale, quality
         */
        public function getImagePreview($image, $path, $settings) {
            $finalImages = null;
            $originalImagePath = $path . '/../original/' . $image->file_name;
            foreach ($settings as $key => $value) {
                if ($value['width'] == null && $value['height'] == null) {
                    $finalImages[$key] = $originalImagePath;
                } elseif ($value['width'] !== null && $value['height'] !== null) {
                    if ($image->width < $value['width'] && $image->height < $value['height']) {
                        $finalImages[$key] = $originalImagePath;
                    } else {
                        $thumbImageName = $value['width'] . 'w_' . $value['height'] . 'h_' . $image->file_name;
                        $thumbImagePath = $path . '/' . $thumbImageName;
                        $created = false;
                        foreach (Finder::findFiles($thumbImageName)->in(WWW_DIR . '/' . $path . '/') as $file) {
                            $created = true;
                            break;
                        }

                        if ($created == true) {
                            $finalImages[$key] = $thumbImagePath;
                        } else {
                            $originalImage = Image::fromFile(WWW_DIR . '/' . $originalImagePath);
                            $imageThumb = $this->createThumb($originalImage, $value['width'], $value['height'], $value['crop'], $value['greyScale'], $value['sharpen']);
                            $imageThumb->save(WWW_DIR . '/' . $thumbImagePath, isset($value['quality']) ? $value['quality'] : 80);
                            $finalImages[$key] = $thumbImagePath;
                        }
                    }
                } elseif ($value['width'] !== null) {
                    if ($image->width < $value['width']) {
                        $finalImages[$key] = $originalImagePath;
                    } else {
                        $thumbImageName = $value['width'] . 'w_' . $image->file_name;
                        $thumbImagePath = $path . '/' . $thumbImageName;
                        $created = false;
                        foreach (Finder::findFiles($thumbImageName)->in(WWW_DIR . '/' . $path . '/') as $file) {
                            $created = true;
                            break;
                        }

                        if ($created == true) {
                            $finalImages[$key] = $thumbImagePath;
                        } else {
                            $originalImage = Image::fromFile(WWW_DIR . '/' . $originalImagePath);
                            $imageThumb = $this->createThumb($originalImage, $value['width'], $value['height'], $value['crop'], $value['greyScale'], $value['sharpen']);
                            $imageThumb->save(WWW_DIR . '/' . $thumbImagePath, isset($value['quality']) ? $value['quality'] : 80);
                            $finalImages[$key] = $thumbImagePath;
                        }
                    }
                } else {
                    if ($image->height < $value['height']) {
                        $finalImages[$key] = $originalImagePath;
                    } else {
                        $thumbImageName = $value['height'] . 'h_' . $image->file_name;
                        $thumbImagePath = $path . '/' . $thumbImageName;
                        $created = false;
                        foreach (Finder::findFiles($thumbImageName)->in(WWW_DIR . '/' . $path . '/') as $file) {
                            $created = true;
                            break;
                        }

                        if ($created == true) {
                            $finalImages[$key] = $thumbImagePath;
                        } else {
                            $originalImage = Image::fromFile(WWW_DIR . '/' . $originalImagePath);
                            $imageThumb = $this->createThumb($originalImage, $value['width'], $value['height'], $value['crop'], $value['greyScale'], $value['sharpen']);
                            $imageThumb->save(WWW_DIR . '/' . $thumbImagePath, isset($value['quality']) ? $value['quality'] : 80);
                            $finalImages[$key] = $thumbImagePath;
                        }
                    }
                }
            }

            return $finalImages;
        }

        /**
         * Vytvori slozku dle zadane cesty
         * @param  string	cesta k vytvoreni
         */
        public function createFolder($path) {
            mkdir($path);
        }

        /**
         * Maze obsah slozky a pripadne i slozku samotnou
         * @param  string	cesta ke slozce jejiz obsah ma byt vymazan
         * @param  bool	true smaze i slozku samotnou
         */
        function emptyFolder($dir, $deleteMe) {
            if (!$dh = @opendir($dir))
                return;
            while (false !== ($obj = readdir($dh))) {
                if ($obj == '.' || $obj == '..')
                    continue;
                if (!@unlink($dir . '/' . $obj))
                    SureRemoveDir($dir . '/' . $obj, true);
            }
            closedir($dh);
            if ($deleteMe) {
                @rmdir($dir);
            }
        }

        /**
         * Prejmenovava slozku - zatim neimplementovano
         * @param  string	stara cesta
         * @param  string	nova cesta
         */
        public function renameFolder($oldName, $newName) {
            sleep(1);
            rename($oldName, $newName);
        }

        public function deleteFile($path) {
            if (file_exists($path)) {
                unlink($path);
            }
        }

        public function clearFileInfo() {
            $this->file = null;
            $this->image = null;
            $this->filename = null;
            $this->path = null;
        }

    }
    