<?php

    namespace AdminModule\Models;

    use Nette\Security as NS;

    class Users extends General\Table {

	private $loggedUser;
        
	public function setInputValue($input, $itemData) {
	    switch ($input['column']) {
		case 'password':
		    $input['value'] = '';

		    break;
		default:
		    $input['value'] = $itemData->$input['column'];
		    break;
	    }
	    return $input;
	}

	public function setUpdateValue($updateArray, $inputData) {
	    switch ($inputData['column']) {
		case 'password':
		    if (!empty($inputData['value'])) {
			$updateArray['password'] = $this->calculateHash($inputData['value']);
		    }
		    break;
		default:
		    $updateArray[$inputData['column']] = $inputData['value'];
		    break;
	    }
	    return $updateArray;
	}

	public function calculateHash($password) {
	    return hash('sha512', $password);
	}
        
        public function detacheItem($id) {
            $user = $this->database->table($this->getTableName())->get($id);
            $user->update(array('companies_id' => null));
        }

	// ##################################### Methods for logged users #####################################

	/**
	 * Sets basic identity when user is logged in
	 * @var int User ID
	 * @return \SuperFCore\Users\BasicIdentity Return Basic identity that contains only user ID
	 */
	public function setBasicIdentity($user) {
	    if ($user instanceof \Nette\Database\Table\ActiveRow) {
		return new \SuperFCore\Users\BasicIdentity($user->id);
	    } else {
		return new \SuperFCore\Users\BasicIdentity($user);
	    }
	}

	/**
	 * Returns users full identity
	 * @var	int User ID
	 * @return  \SuperFCore\Users\BasicIdentity Return Basic identity that contains only user ID
	 */
	public function getFullIdentity($userID) {
	    if (!$this->loggedUser) {
		$this->loggedUser = $this->database->table($this->getTableName())->where('id', $userID)->fetch('id', 'active', 'lang', 'name', 'email', 'role');
		if (!$this->loggedUser) {
		    return null;
		} elseif ($this->loggedUser->active !== 1) {
		    return null;
		}
	    }

	    $userArray = $this->loggedUser->toArray();
	    unset($userArray['password']);
	    unset($userArray['activekey']);

	    // Roles
	    $roles = explode(', ', $this->loggedUser->role);
	    return new NS\Identity($this->loggedUser->id, $roles, $userArray);
	}

    }
    