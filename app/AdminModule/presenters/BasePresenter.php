<?php

namespace AdminModule;

use MobileDetect\MobileDetect,
    Nette\Diagnostics\Debugger;

class BasePresenter extends \BasePresenter {

    protected function startup() {
	parent::startup();

	// Backend translator
	$this->translator->setLocale($this->backLang);
	$this->template->setTranslator($this->translator);

	// If this is ajax, snippets are invalidated
	if ($this->isAjax()) {
	    $this->getAjaxReloadSession();
	}
    }

    protected function beforeRender() {
	parent::beforeRender();
        // Loads up title and flashes
	if ($this->isAjax()) {
	    $this->redrawControl('title');
	    $this->redrawControl('flash');
	}
    }

    public function getAjaxReloadSession() {
	$ajaxSession = $this->getSession('ajax');
	if (!empty($ajaxSession->snippets)) {
	    foreach ($ajaxSession->snippets as $snippet) {
		$this->redrawControl($snippet);
	    }
	    unset($ajaxSession->snippets);
	} 
	
        // page reload script for menu items
	if ($ajaxSession->page !== $this->getAction(true)) {
	    $this->redrawControl('page');
	}
	$ajaxSession->page = $this->getAction(true);
    }

    public function setAjaxReloadSession($snippets) {
	$ajaxSession = $this->getSession('ajax');
	if (!empty($ajaxSession->snippets)) {
	    $ajaxSession->snippets = array_merge($ajaxSession->snippets, $snippets);
	} else {
	    $ajaxSession->snippets = $snippets;
	}
    }

}
