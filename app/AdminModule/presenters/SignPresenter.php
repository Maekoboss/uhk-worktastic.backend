<?php

namespace AdminModule;

use Nette\Application\UI\Form,
    Nette\Security as NS;

class SignPresenter extends BasePresenter {

    /** @persistent */
    public $backlink = '';

    protected function createComponentFormSignIn($name) {
	return new VisualComponents\FormSignIn;
    }
    
    protected function createComponentFormRegister($name) {
	return new VisualComponents\FormRegister;
    }

    public function actionOut() {
	$this->getUser()->logout(true);
	$session = $this->getSession("KCFINDER");
	$session->remove();
	$this->flashMessage('Byl jste odhlášen.');
	$this->redirect('in');
    }

}
