<?php

    namespace AdminModule;

    class SecuredPresenter extends BasePresenter {

        protected $mainMenu;

        protected function startup() {
            parent::startup();

            // If no identity user is logged out
            if (!$this->user->getIdentity()) {
                $this->ajaxSafeLogout();
            }

            // User authorization to access Backend
            if (!$this->user->isLoggedIn() || !$this->user->isAllowed('Admin:')) {
                if ($this->user->logoutReason == \Nette\Http\UserStorage::INACTIVITY) {
                    $this->flashMessage('you-was-logged-out-inactivity');
                }

                if (!$this->user->isAllowed('Admin:') && $this->user->isLoggedIn()) {
                    $this->flashMessage('not-enough-permission');
                }

                $this->ajaxSafeLogout();
            }
        }

        public function ajaxSafeLogout() {
            if ($this->isAjax()) {
                $this->payload->hardRedirect = true;
                $this->payload->link = $this->link('//:Admin:Sign:in', array('backlink' => $this->storeRequest()));
                $this->sendPayload();
            } else {
                $this->redirect(':Admin:Sign:in', array('backlink' => $this->storeRequest()));
            }
        }

        public function createComponentStructureMenu() {
            $mainMenu = $this->page->getMenu(NULL, 'companyAdmin');
            return new \AdminModule\VisualComponents\MenuSidebar($mainMenu, $this->translator);
        }

        public function createComponentOptionsMenu() {
            return new \AdminModule\VisualComponents\MenuSidebar($this->page->getMenu(NULL, 'optionsMenu'), $this->translator);
        }

    }
    