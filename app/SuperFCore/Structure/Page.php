<?php

/**
 * Zakladni trida predstavujici stranku v SuperFast CMS
 * Zajistuje nahrani vsech zakladnich informaci o strance
 *
 * @author Marko Rumpli
 */

namespace SuperFCore\Structure;

class Page extends \Nette\Object {

    /** @var \Nette\Database\Context */
    private $database;

    /** @var \Nette\Application\UI\Presenter */
    private $presenter;
    private $pagePresenterName;
    private $lang;
    //------------------------------- Structure -------------------------------
    /** @var \Nette\Database\Table\Selection */
    private $structureData;
    private $structureArray = array();
    //------------------------------- Page Data -------------------------------
    /** @var \Nette\Database\Table\ActiveRow */
    public $data;

    /** @var \Nette\ArrayHash */
    public $options;
    public $title;
    public $description;

    /** @var \Nette\Database\Table\ActiveRow */
    public $mainPost = null;

    /** @var \Nette\ArrayHash */
    public $expansions = null;

    /** @var \Nette\ArrayHash */
    public $images = null;

    public function __construct($database, $presenter, $pagePresenterName, $lang = 'cz', $sort = 'id') {
	$this->database = $database;
	$this->presenter = $presenter;
	$this->pagePresenterName = $pagePresenterName;
	$this->lang = $lang;

	$this->structureData = $database->table('structure')->where('lang', $lang);

	$this->pagePresenterName = $pagePresenterName;
	foreach ($this->structureData as $row) {
	    if ($row->presenter == $this->pagePresenterName || $row->bPresenter == $this->pagePresenterName) {
		$this->setPageData($row);
	    }
	}
    }

    public function getMenu($parentID, $mainKey = 'mainMenu', $level = 1, $levelStop = 1, $active = false) {
	if ($level <= $levelStop) {
	    $result = new \Nette\Utils\ArrayHash();
	    $menuItems = $this->database->table('structure')
		    ->where('lang', $this->lang)
		    ->where(array(
				'parent_id' => $parentID,
				'main_key' => $mainKey
		));
	    foreach ($menuItems as $item) {
		$activeState = $active;
		$level++;
		$id = $item->id;
		$result->offsetSet($id, new \Nette\Utils\ArrayHash());
		$result->$id->offsetSet('data', $item);
		if ($activeState == false && $this->pagePresenterName == $item->bPresenter) {
		    $activeState = true;
		}
		$result->$id->offsetSet('active', $activeState);
		$result->$id->offsetSet('children', $this->getMenu($item->id, $mainKey, $level, $levelStop, $activeState));
	    }
	    return $result;
	} else {
	    return null;
	}
    }

    public function setPageData($data) {
	$this->data = $data;
	if ($this->data) {

	    $this->options = \Nette\ArrayHash::from($this->database->table('options')->where('lang', $this->lang)->fetchPairs('main_key', 'value'));

	    $this->title = $this->data->seo_title;
	    $this->description = $this->data->description;
	    //$this->mainPost = $this->data->related('posts')->where('posts.main_key', 'mainPost')->fetch();

	    /*$expansions = $this->data->related('expansions')->fetchAll();
	    if ($expansions) {
		$this->expansions = new \Nette\ArrayHash();
		foreach ($expansions as $expansion) {
		    $expansionValue = 'value_' . $expansion->type;
		    $this->expansions->offsetSet($expansion->main_key, $expansion->$expansionValue);
		}
	    }*/

	    $images = $this->data->related('images')->fetchPairs('main_key');
	    if ($images) {
		$this->images = \Nette\ArrayHash::from($images);
	    }
	}
    }

}
