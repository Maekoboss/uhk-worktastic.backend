<?php

/**
 * Prekladac do RS SuperFast CMS
 *
 * @author Marko Rumpli
 */

namespace SuperFCore\Utils;

use Nette\Caching\Cache;

class Translator implements \Nette\Localization\ITranslator {

    /** @var \Nette\Database\Context */
    private $database;

    /** @var Cache */
    private $cache;
    private $lang = 'cz';
    private $source = array();

    public function __construct(\Nette\Database\Context $database, \Nette\Caching\Storages\FileStorage $storage) {
	$this->database = $database;
	$this->cache = new Cache($storage, 'translator');
    }

    public function setLocale($lang = 'cz') {
	$this->lang = $lang;
	$this->source = $this->getSource();
    }

    public function getSource() {
	$returnSource = new \Nette\ArrayHash;
	$newSource = new \Nette\ArrayHash;
	$newSourceCount = 0;
	$cachedSource = $this->cache->load('source_' . $this->lang);
	$cachedSourceCount = $this->cache->load('source_' . $this->lang . '_count');

	if ($cachedSource && $cachedSourceCount) {
	    $currentSourceCount = (int) $this->database->query('select count(id) from translations where lang = ?', $this->lang)->fetch()['count(id)'];
	    if ($currentSourceCount != $cachedSourceCount) {
		$newSourceData = $this->database->table('translations')->where('lang', $this->lang);
		$newSourceCount = $newSourceData->count();
		foreach ($newSourceData as $key => $data) {
		    $newSource->offsetSet($data->key, \Nette\ArrayHash::from($data));
		}
		$returnSource = $newSource;
		$this->cache->save('source_' . $this->lang, $newSource, array(Cache::TAGS => array('translations')));
		$this->cache->save('source_' . $this->lang . '_count', $newSourceCount, array(Cache::TAGS => array('translations')));
	    } else {
		$returnSource = $cachedSource;
	    }
	} else {
	    $newSourceData = $this->database->table('translations')->where('lang', $this->lang);
	    $newSourceCount = $newSourceData->count();
	    foreach ($newSourceData as $key => $data) {
		$newSource->offsetSet($data->key, \Nette\ArrayHash::from($data));
	    }
	    $returnSource = $newSource;
	    $this->cache->save('source_' . $this->lang, $newSource, array(Cache::TAGS => array('translations')));
	    $this->cache->save('source_' . $this->lang . '_count', $newSourceCount, array(Cache::TAGS => array('translations')));
	}
	return $returnSource;
    }

    public function translate($message, $count = null, $search = null, $replace = null) {
	$finalMessage = $message;
	if (array_key_exists($message, $this->source)) {
	    if (is_numeric($count) && (strpos($message, '*NumberFix*') == false)) {
		$count = $this->getCountVersion($count);
		$finalMessage = $this->source->$message->$count;
	    } else {
		$finalMessage = $this->source->$message->v1;
	    }
	    // search slozi pro vyraz, ktery se ma najit v prekladu a replace ho nahrazuje $search = '%pocet%', $replace = '5'
	    if ($search && ($replace || $replace == 0)) {
		$finalMessage = str_replace($search, $replace, $finalMessage);
	    }
	    return $finalMessage;
	} else {
	    return $finalMessage;
	}
    }

    public function getCountVersion($count) {
	if ($this->lang == 'cz') {
	    if ($count == 1) {
		return 'v1';
	    } elseif ($count > 1 && $count < 5) {
		return 'v2';
	    } else {
		return 'v3';
	    }
	} elseif ($this->lang == 'en') {
	    if ($count == 1) {
		return 'v1';
	    } else {
		return 'v2';
	    }
	}
    }

}
