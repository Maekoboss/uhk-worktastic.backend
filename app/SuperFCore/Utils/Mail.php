<?php

/*
 * This mailer uses Mailgun to send emails...
 * Mailgun version 1.7.2
 */

namespace SuperFCore\Utils;

use Mailgun as MG;

class Mail {

    /** @var MG\Mailgun */
    private $mailer;
    private $apiKey = 'key-95c2c7580896680256a2c3a7d133f268';
    private $domain = 'sandbox336088d0c49b4071b8c521d38a8c3471.mailgun.org';
    // Mail content
    private $from;
    private $to;
    private $cc;
    private $subject;
    private $htmlBody;
    private $textBody;

    /** 
     * @param string  From address Name <name@email.com>
     * @param string | array  To address could be also array name => address
     * @param string  Subject
     * @param string(html)    HTML body of the message
    */
    public function __construct($from, $to, $subject, $htmlBody, $cc = null) {
	$this->mailer = new MG\Mailgun($this->apiKey);
	$this->from = $from;
	$this->to = $this->createRecepients($to);
	$this->subject = $subject;
	$this->htmlBody = $htmlBody;
	$this->textBody = $this->buildText($this->htmlBody);
	return $this;
    }

    public function sendMail() {
	$postData = array(
	    'from' => $this->from,
	    'to' => $this->to,
	    'subject' => $this->subject,
	    'html' => $this->htmlBody,
	    'text' => $this->textBody
	);
	$result = $this->mailer->sendMessage($this->domain, $postData);
	return $result;
    }

    public function createRecepients($recepients) {
	$result = '';
	if (is_array($recepients)) {
	    $count = count($recepients);
	    $i = 0;
	    foreach ($recepients as $name => $email) {
		if (is_string($name)) {
		    $result .= $name . ' <' . $email . '>';
		} else {
		    $result .= $email;
		}

		if ($i < $count) {
		    $result .= ', ';
		}
	    }
	} else {
	    $result = $recepients;
	}

	return $result;
    }

    public function setTextBody($textBody) {
	$this->textBody = $textBody;
	return $this;
    }

    /**
     * Builds text content from HTML - taken from Nette.
     * @return string
     */
    protected function buildText($html) {
	$text = \Nette\Utils\Strings::replace($html, array(
		    '#<(style|script|head).*</\\1>#Uis' => '',
		    '#<t[dh][ >]#i' => " $0",
		    '#[\r\n]+#' => ' ',
		    '#<(/?p|/?h\d|li|br|/tr)[ >/]#i' => "\n$0",
	));
	$text = html_entity_decode(strip_tags($text), ENT_QUOTES, 'UTF-8');
	$text = \Nette\Utils\Strings::replace($text, '#[ \t]+#', ' ');
	return trim($text);
    }

}
