<?php

/**
 * Zajišťuje zobrazení náhledů obrázků
 *
 * @author Marko Rumpli
 */

namespace SuperFCore\Utils;

class Helpers {

    public $fileProcessor;

    /** @var \AdminModule\Gallery */
    public $gallery;
    public $path = '/user_uploads/';
    public $months;
    public $days;
    public $currency;

    public function __construct(\AdminModule\Models\FileProcessor $fileProcessor, \AdminModule\Models\Gallery $gallery, $lang = 'cz') {
	$this->fileProcessor = $fileProcessor;
	$this->gallery = $gallery;

	// Setting dates
	if ($lang == 'cz' || $lang == 'sk') {
	    $this->months = array(1 => 'Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listipad', 'Prosinec');
	    $this->days = array(0 => 'Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota');
	} else {
	    $this->months = array(1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	    $this->days = array(0 => 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	}

	// Settings currencies
	if ($lang == 'cz') {
	    $this->currency = ' ,- Kč';
	} else {
	    $this->currency = ' $';
	}
    }

    public function getPreview($image, $width = null, $height = null, $crop = true, $greyScale = false, $sharpen = false, $quality = 100) {
	if (is_a($image, '\Nette\Utils\ArrayHash')) {
	    $image = array_values((array) $image)[0];
	}
	
	if (is_a($image, '\Nette\Database\Table\ActiveRow')) {
	    $parentModuleInfo = $this->gallery->getParentModuleInfo($image);
	    $path = $this->path . 'gallery/' . (key($parentModuleInfo)) . '_' . array_values($parentModuleInfo)[0] . '/forWeb';
	    $settings = array(
		0 => array(
		    'width' => $width,
		    'height' => $height,
		    'crop' => $crop,
		    'greyScale' => $greyScale,
		    'sharpen' => $sharpen,
		    'quality' => $quality
		)
	    );
	    return $this->fileProcessor->getImagePreview($image, $path, $settings)[0];
	} else {
	    return null;
	}
    }

    public function datesInWords($dateTime) {
	if (!is_a($dateTime, 'DateTime')) {
	    $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
	}
	if ($dateTime) {
	    $day = $dateTime->format('w');
	    $month = $dateTime->format('n');
	    return $this->days[$day].', '.$this->months[$month].' '.$dateTime->format('j').', '.$dateTime->format('Y');
	} else {
	    return '';
	}
    }

    public function monthAsWord($dateTime) {
	if (!is_a($dateTime, 'DateTime')) {
	    $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
	}

	if ($dateTime) {
	    $month = $dateTime->format('n');
	    return $this->months[$month];
	} else {
	    return '';
	}
    }

    public function currency($number) {
	if (is_numeric($number)) {
	    $finalPrice = number_format($number, 0, '', ' ') . $this->currency;
	    return $finalPrice;
	} else {
	    return $number;
	}
    }

    public function surface($number) {
	$surface = $number . ' m<sup>2</sup>';
	return $surface;
    }

}
