<?php

namespace SuperFCore\Users;

class BasicIdentity implements \Nette\Security\IIdentity {
    
    private $id;

    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getRoles() {
        return array();
    }

}
