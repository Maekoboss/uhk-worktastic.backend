<?php

    namespace SuperFCore\Users;

    use Nette\Http\Session,
	Nette\Http\UserStorage as NetteUserStorage,
	Nette\Security\IIdentity;

    class UserStorage extends NetteUserStorage {

	/** @var \AdminModule\Models\Users */
	private $usersModel;

	public function __construct(Session $sessionHandler, \AdminModule\Models\Users $usersModel) {
	    parent::__construct($sessionHandler);
	    $this->usersModel = $usersModel;
	}

	/**
	 * Sets the user identity.
	 * @return UserStorage Provides a fluent interface
	 */
	public function setIdentity(IIdentity $identity = NULL) {
	    if ($identity !== NULL) {
		$identity = new BasicIdentity($identity->getId());
	    }
	    return parent::setIdentity($identity);
	}

	/**
	 * Returns current user identity, if any.
	 * @return IIdentity|NULL
	 */
	public function getIdentity() {
	    $identity = parent::getIdentity();
	    // Refresh identity data
	    if ($identity instanceof BasicIdentity) {
		return $this->usersModel->getFullIdentity($identity->getId());
	    }

	    return $identity;
	}

    }
    