<?php

/**
 * Dependent select box
 *
 * @author Marko Rumpli
 */

namespace SuperFCore\Forms;

class DependentSelect extends \Nette\Forms\Controls\SelectBox {

    private $parentBoxes = null;

    /** @var \Nette\Application\UI\Control */
    private $appControl;
    private $handler;

    public function __construct($label, $parentBoxes, $handler) {
	parent::__construct($label, null, null);
	if (is_array($parentBoxes)) {
	    $this->parentBoxes = $parentBoxes;
	} else {
	    $this->parentBoxes = array($parentBoxes);
	}
	$this->handler = $handler;
    }

    public function attached($form) {
	parent::attached($form);
	$this->appControl = $this->lookup('\Nette\Application\UI\Control', true);
	// slouzi jako selector, do kterych selectboxu se maji vlozit nova data
	$this->setAttribute('data-parent-selectboxes', $this->getParentIDsJson($this->parentBoxes));
	$this->setAttribute('data-dependent-selectbox', true);

	$parentValues = array();
	foreach ($this->parentBoxes as $parentBoxName => $parentBox) {
	    //slouzi jako link odkud se maji natahnout nova data + podle adresy se groupuje, ktere hodnoty se poslou jako parametry.
	    $parentBox->setAttribute('data-update-link', $this->appControl->link($this->handler.'!'))
		    // Slouzi pro generovani spravneho nazvu parametru komponenty do URL, aby propadl do spravne komponenty
		    ->setAttribute('data-update-link-controlname', $this->appControl->name)
		    // Slouzi pro generovani spravneho nazvu parametru inputu do URL, aby se v handleru dal spravne priradit
		    ->setAttribute('data-update-link-paramname', $parentBoxName);
	    $value = $parentBox->getValue();
	    if ($value) {
		$parentValues[] = $value;
	    }
	}

	if ($parentValues) {
	    // TODO - dodelat neco poradnyho
            $handler = 'handle'.ucwords($this->handler);
	    $items = $this->appControl->$handler(1, isset($parentValues[0]) ? $parentValues[0] : null, isset($parentValues[1]) ? $parentValues[1] : null, $this->getControl()->attrs['name']);
	    $this->setItems($items);
	}
    }

    public function getParentIDsJson($parents) {
	$finalArray = array();
	foreach ($parents as $parent) {
	    $finalArray[] = $parent->getHtmlId();
	}
	return json_encode(array('id' => $finalArray));
    }

}
