<?php

    /**
     * DateTimePicker
     *
     * @author Marko
     */

    namespace SuperFCore\Forms;

    class DateTimePicker extends \RadekDostal\NetteComponents\DateTimePicker\DateTimePicker {

        public function __construct($label = NULL, $cols = NULL, $maxLength = NULL) {
            parent::__construct($label, $cols, $maxLength);
            parent::setFormat('Y-m-d H:i:s');
        }

    }
    