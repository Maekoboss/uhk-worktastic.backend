<?php

class ErrorPresenter extends BasePresenter {

    public function renderDefault($exception) {
	// Browser Language to show the right traslation
	$finalLang = 'en';
	foreach (explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']) as $lang) {
	    $pattern = '/^(?P<primarytag>[a-zA-Z]{2,8})' .
		    '(?:-(?P<subtag>[a-zA-Z]{2,8}))?(?:(?:;q=)' .
		    '(?P<quantifier>\d\.\d))?$/';

	    $splits = array();
	    if (preg_match($pattern, $lang, $splits)) {
		if (!empty($splits['primarytag'])) {
		    if ($splits['primarytag'] == 'cs') {
			$finalLang = 'cz';
			break;
		    }
		}
	    }
	}

	$this->presenter->translator->setLocale($finalLang);

	// Error handling
	$code = $exception->getCode();
	$message = $this->presenter->translator->translate('server-error-appeared');
	if ($code == 400 || $code == 404 || $code == 405) {
	    $message = $this->presenter->translator->translate('requested-site-was-not-found');
	} elseif ($code == 401 || $code == 401) {
	    $message = $this->presenter->translator->translate('you-dont-have-permisson-for-this-page');
	}

	$this->template->code = $code;
	$this->template->message = $message;

	if ($this->isAjax()) {
	    $this->payload->error = TRUE;
	    $this->terminate();
	}
    }

}
