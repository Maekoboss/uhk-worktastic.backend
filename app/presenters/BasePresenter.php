<?php

    use Nette\Application\UI\Presenter,
        Nette\Caching\Cache;

    /**
     *
     * @property-read \SystemContainer $context
     */
    abstract class BasePresenter extends Presenter {

        // Loads basic dependencies
        /** @inject @var Nette\Database\Context */
        public $database;
        /** @inject @var SuperFCore\Utils\Translator */
        public $translator;
        
        
        // Static variables
        /** @var SuperFCore\Structure\Page */
        protected $page;
        /** @inject @var \SuperFCore\Utils\Helpers */
        protected $superFHelpers;
        /** @persistent */
        public $lang = 'en';
        /** var @persistent */
        public $backLang = 'en';

        /** @var \Nette\Utils\DateTime */
        protected $currentDate;

        protected function startup() {
            parent::startup();

            // Language & translator
            if ($this->getParam('lang') !== null) {
                $this->lang = $this->getParam('lang');
            }
            $this->translator->setLocale($this->lang);
            $this->template->setTranslator($this->translator);
            $this->setModulesLang();

            // Page structure
            $this->page = new SuperFCore\Structure\Page($this->database, $this->getPresenter(), ':' . $this->getPresenter()->name . ':', $this->lang, 'sort, id');

            // Helpers, Forms and others
            \Nette\Forms\Form::extensionMethod('addDateTimePicker', function(Nette\Application\UI\Form $_this, $name, $label, $cols = NULL, $maxLength = NULL) {
                return $_this[$name] = new SuperFCore\Forms\DateTimePicker($label, $cols, $maxLength);
            });

            \Nette\Forms\Form::extensionMethod('addDependentSelect', function(Nette\Application\UI\Form $_this, $name, $label, $parentBox, $handler) {
                return $_this[$name] = new SuperFCore\Forms\DependentSelect($label, $parentBox, $handler);
            });
            $this->template->registerHelper('getPreview', array($this->superFHelpers, 'getPreview'));
            $this->template->registerHelper('getFileUrl', array($this->superFHelpers, 'getFileUrl'));
            $this->template->registerHelper('datesInWords', array($this->superFHelpers, 'datesInWords'));
            $this->template->registerHelper('monthAsWord', array($this->superFHelpers, 'monthAsWord'));
            $this->template->registerHelper('currency', array($this->superFHelpers, 'currency'));
            $this->template->registerHelper('surface', array($this->superFHelpers, 'surface'));
            $this->currentDate = new \Nette\Utils\DateTime;

            // Push basic data to template
            $this->template->lang = $this->lang;
            $this->template->page = $this->page;
            $this->template->currentDate = $this->currentDate;
        }

        public function setModulesLang() {
            $this->context->getService('structure')->setLang($this->lang, $this->backLang);
            $this->context->getService('gallery')->setLang($this->lang, $this->backLang);
            $this->context->getService('translations')->setLang($this->lang, $this->backLang);
        }

    }
    