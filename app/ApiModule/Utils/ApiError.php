<?php

namespace ApiModule\Utils;

class ApiError extends \Exception {

    public function __construct($message, $code) {
	parent::__construct($message, $code);
    }

}
