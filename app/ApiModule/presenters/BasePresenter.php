<?php

namespace ApiModule;

use \Drahak\Restful\Application\UI\ResourcePresenter,
    ApiModule\Utils;

class BasePresenter extends ResourcePresenter {

    /** @inject @var \Nette\Database\Context */
    public $database;
    protected $validator;
    protected $device;
    protected $user;

    protected function startup() {
	parent::startup();
	$this->dbDump($this->input);
	$this->validator = new \Drahak\Restful\Validation\Validator;
	$headers = $this->getHttpRequest()->getHeaders();

	// Device-id must be always send
	if (empty($headers['device-id'])) {
	    $error = new Utils\ApiError('We were unable to resolve your device ID', 400);
	    $this->sendErrorResource($error);
	}

	// Resolving identities based od auth-token and device-id
	if (!empty($headers['auth-token'])) {
	    $authToken = $headers['auth-token'];
	    $this->user = $this->database->table('users')->get(array('auth_token' => $authToken));
	    if (!$this->user) {
		$error = new Utils\ApiError('We were unable to resolve your identity. You were logged out', 400);
		$this->sendErrorResource($error);
	    }
	}

	$deviceID = $headers['device-id'];
	$this->device = $this->database->table('devices')->get(array('device_id' => $deviceID));
	// if no device, it is created
	if (!$this->device) {
	    $this->device = $this->database->table('devices')->insert(array('device_id' => $deviceID));
	}
    }

    // Override of the error create method
    protected function createErrorResource(\Exception $e) {
	$resource = $this->resourceFactory->create(array(
	    'ErrorMsg' => $e->getMessage()
	));

	if (isset($e->errors) && $e->errors) {
	    $resource->errors = $e->errors;
	}

	return $resource;
    }

    public function dbDump($item) {
	$this->database->table('dev_json')->insert(
		array(
		    'url' => $this->getAction(true),
		    'text' => \Tracy\Debugger::dump($item, true)
	));
    }

}
