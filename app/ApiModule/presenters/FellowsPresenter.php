<?php

    namespace ApiModule;

    use \Drahak\Restful\IResource,
        \Drahak\Restful\Application\UI\ResourcePresenter;

    class FellowsPresenter extends BasePresenter {

        /**
         * @GET user/fellows
         */
        public function actionRead() {
            if (!empty($this->user->companies_id)) {
                $company = $this->user->companies;
                $prevDay = new \Nette\Utils\DateTime;
                $prevDay->modify('-1 day');
                $openedSessions = $company->related('sessions_opened')->where(array(
                    'start >= ?' => $prevDay->format('Y-m-d H:i:s'),
                    'users_id <> ?' => $this->user->id
                    ))->fetchAll();
                if (!empty($openedSessions)) {
                    $fellowsArray = array();
                    foreach ($openedSessions as $session) {
                        $fellowData = array(
                            'Name' => $session->users->name,
                            'ActivityID' => $session->activities_id,
                            'FeelingID' => $session->feelings_id,
                            'Photo' => "http://uhk.worktasticapp.com/resources/user-placeholder.png"
                        );
                        $fellowsArray[$session->users_id] = $fellowData;
                    }
                    $returnArray = array();
                    foreach ($fellowsArray as $fellow) {
                        $returnArray[] = $fellow;
                    }
                    $this->resource->Fellows = $returnArray;
                    $this->sendResource(IResource::JSON);
                }
            }
            $this->resource->Fellows = array();
            $this->sendResource(IResource::JSON);
        }

    }
    