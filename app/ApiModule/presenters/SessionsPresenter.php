<?php

namespace ApiModule;

use \Drahak\Restful\IResource,
    \Drahak\Restful\Application\UI\ResourcePresenter;

class SessionsPresenter extends BasePresenter {

    private $currentDate;
    
    /**
     * @POST homepage/sessions
     */
    public function actionCreate() {
	$sessionPost = array();
	$activitiesPost = array();
	$feelingsPost = array();
	try {
	    // SessionItem
	    if (!empty($this->input->SessionItem)) {
		$sessionPost = $this->input->SessionItem;
	    } else {
		throw new Utils\ApiError('We received invalid data', 400);
	    }

	    // Activities
	    if (!empty($this->input->Activities)) {
		$activitiesPost = $this->input->Activities;
	    } else {
		throw new Utils\ApiError('We received invalid data', 400);
	    }

	    // Feelings
	    if (!empty($this->input->Feelings)) {
		$feelingsPost = $this->input->Feelings;
	    } else {
		throw new Utils\ApiError('We received invalid data', 400);
	    }

	    // Save Data to DB
	    $savedData = $this->SaveSession($sessionPost);
	    $savedSession = $savedData[0];
	    $sessionFinished = $savedData[1];
	    if ($savedSession) {
		// Save Activities
		if ($sessionFinished == true) {
		    $this->SaveActivities($activitiesPost, $savedSession);
		} else {
		    $activeActivity = $activitiesPost[0]['ActivityID'];
		}
		
		// Save Feelings
		if ($sessionFinished == true) {
		    $this->SaveFeelings($feelingsPost, $savedSession);
		} else {
		    $activeFeeling = $feelingsPost[0]['FeelingID'];
		}
		
		// Save opened session data
		if ($sessionFinished == false) {
		    $this->SaveOpenedSession($savedSession, $activeActivity, $activeFeeling);
		} else {
		    $this->ClearOpenedSessions();
		}

		$this->resource->ServerID = $savedSession->id;
		$this->sendResource(IResource::JSON);
	    }
	} catch (Utils\ApiError $e) {
	    $this->sendErrorResource($e);
	}
    }
    
    /**
     * @PUT homepage/sessions
     */
    public function actionUpdateCurrentSession() {
        $openedSession = $this->device->related('sessions_opened')->order('start DESC')->fetch();
        if ($openedSession) {
            $updateArray = array();
            if ($this->input->ActivityID > 0) {
                $updateArray['activities_id'] = $this->input->ActivityID;
            } elseif ($this->input->FeelingID > 0) {
                $updateArray['feelings_id'] = $this->input->FeelingID;
            }
            $openedSession->update($updateArray);
        }
    }

    public function SaveSession($sessionPost) {
	$this->currentDate = new \Nette\Utils\DateTime();
	$appStart = $this->ConvertToDate($sessionPost['StartDate']);
	$appStop = $this->ConvertToDate($sessionPost['StopDate']);
	$sessionData = null;

	if (!empty($sessionPost['ServerID'])) {
	    $sessionData = $this->database->table('sessions')->get($sessionPost['ServerID']);
	}

	// New Session should be created
	if (!$sessionData) {
	    $insertArray = array(
		'devices_id' => $this->device->id,
		'users_id' => !empty($this->user) ? $this->user->id : null,
                'companies_id' => !empty($this->user->companies_id) ? $this->user->companies_id : null,
		'app_start' => $appStart ? $appStart->format('Y-m-d H:i:s') : null,
		'app_stop' => $appStop ? $appStop->format('Y-m-d H:i:s') : null,
	    );
	    if (!$appStop) {
		$insertArray['start'] = $this->currentDate->format('Y-m-d H:i:s');
	    } else {
		$insertArray['stop'] = $this->currentDate->format('Y-m-d H:i:s');
	    }
	    $sessionData = $this->database->table('sessions')->insert($insertArray);
	} else {
	    $updateArray = array(
		'users_id' => !empty($this->user) ? $this->user->id : null,
                'companies_id' => !empty($this->user->companies_id) ? $this->user->companies_id : null,
		'app_stop' => $appStop ? $appStop->format('Y-m-d H:i:s') : null,
		'stop' => $this->currentDate->format('Y-m-d H:i:s')
	    );
	    $sessionData->update($updateArray);
	}

	return array($sessionData, $appStop);
    }
    
    public function SaveActivities($activitiesPost, $sessionData) {
	foreach ($activitiesPost as $activity) {
	    $insertArray = array(
		'sessions_id' => $sessionData->id,
		'activities_id' => $activity['ActivityID'],
		'start' => $this->ConvertToDate($activity['StartDate'])
	    );
	    $this->database->table('sessions_activities')->insert($insertArray);
	}
    }
    
    public function SaveFeelings($feelingsPost, $sessionData) {
	foreach ($feelingsPost as $feeling) {
	    $insertArray = array(
		'sessions_id' => $sessionData->id,
		'feelings_id' => $feeling['FeelingID'],
		'start' => $this->ConvertToDate($feeling['StartDate'])
	    );
	    $this->database->table('sessions_feelings')->insert($insertArray);
	}
    }

    public function SaveOpenedSession($sessionData, $activity, $feeling) {
	// Update / Create opened session
	$prevDay = clone $this->currentDate;
	$prevDay->modify('-1 day');
	$openedSession = $this->database->table('sessions_opened')->where(
			array(
			    'devices_id' => $this->device->id,
			    'users_id' => !empty($this->user) ? $this->user->id : null,
			    'start >= ?' => $prevDay->format('Y-m-d H:i:s')
		))->fetch();
	if ($openedSession) {
	    $udapteArray = array(
		'start' => $sessionData->start,
		'activities_id' => $activity,
		'feelings_id' => $feeling
	    );
	    $openedSession->update($udapteArray);
	} else {
	    $insertArray = array(
		'devices_id' => $this->device->id,
		'users_id' => !empty($this->user) ? $this->user->id : null,
                'companies_id' => !empty($this->user->companies_id) ? $this->user->companies_id : null,
		'start' => $sessionData->start,
		'activities_id' => $activity,
		'feelings_id' => $feeling
	    );
	    $this->database->table('sessions_opened')->insert($insertArray);
	}
    }
    
    public function ClearOpenedSessions() {
	$this->database->table('sessions_opened')->where('devices_id', $this->device->id)->delete();
    }

    public function ConvertToDate($date) {
        $date = substr($date, 0, strpos($date, '.'));
	$finalDate = \Nette\Utils\DateTime::createFromFormat('Y-m-d\TH:i:s', $date);
	if ($finalDate != false) {
	    if (!($finalDate->format('Y') > '2000')) $finalDate = false;
	}
	return $finalDate != false ? $finalDate : null;
    }

}
