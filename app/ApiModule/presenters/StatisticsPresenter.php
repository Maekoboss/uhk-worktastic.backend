<?php

    namespace ApiModule;

    use \Drahak\Restful\IResource,
        \Drahak\Restful\Application\UI\ResourcePresenter;

    class StatisticsPresenter extends BasePresenter {

        const SECOND = 1;
        const MINUTE = 60 * self::SECOND;
        const HOUR = 60 * self::MINUTE;

        /**
         * @GET user/statistics
         */
        public function actionRead() {
            $statsData = $this->getStats($this->input->type, $this->input->filter);
            $this->resource->Stats = $statsData;
            $this->sendResource(IResource::JSON);
        }

        public function validateRead() {
            $this->input->field('type')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Type is required')
                    ->addRule(\Drahak\Restful\Validation\IValidator::IS_IN, 'Type value is out of available range', array('TWeek', 'LWeek', 'TMonth', 'LMonth'));
            $this->input->field('filter')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Filter is required')
                    ->addRule(\Drahak\Restful\Validation\IValidator::IS_IN, 'Filter is out of available range', array('Activities', 'Feelings'));
        }

        public function getStats($type, $filter) {
            $whereArray = array(
                'app_start IS NOT NULL', 'app_stop IS NOT NULL'
            );

            // Date ranges
            $datesRange = $this->getDatesRange($type);
            $whereArray['app_start >= ?'] = $datesRange[0];
            $whereArray['app_start <= ?'] = $datesRange[1];

            // Anonymous data or for registered user
            if ($this->user) {
                $whereArray['users_id'] = $this->user->id;
            } else {
                $whereArray['users_id'] = null;
                $whereArray['devices_id'] = $this->device->id;
            }

            // Build up the final data
            $finalStats = array();
            $sessions = $this->database->table('sessions')->where($whereArray)->fetchAll();
            $overAllTime = 0;
            $detailTime = array();
            if ($sessions) {
                foreach ($sessions as $session) {
                    // Overall
                    $sessionStart = $session->app_start;
                    $sessionStop = $session->app_stop;
                    $overAllTime = $overAllTime + ($sessionStop->getTimestamp() - $sessionStart->getTimestamp());

                    // Detail statistics
                    if ($filter == 'Activities') {
                        $detailTable = 'sessions_activities';
                        $infoTableName = 'activities';
                    } elseif ($filter == 'Feelings') {
                        $detailTable = 'sessions_feelings';
                        $infoTableName = 'feelings';
                    }
                    $infoTable = $this->database->table($infoTableName);
                    $detailData = $session->related($detailTable)->order('start DESC')->fetchAll();
                    if ($detailData) {
                        $previousStart = $sessionStop;
                        foreach ($detailData as $detail) {
                            $detailTime[$detail->$infoTableName->id] = (!empty($detailTime[$detail->$infoTableName->id]) ? $detailTime[$detail->$infoTableName->id] : 0) + ($previousStart->getTimestamp() - $detail->start->getTimestamp());
                            $previousStart = $detail->start;
                        }
                        arsort($detailTime);
                    }
                }
            }
            $finalStats[] = array(
                'Value' => $this->convertItervalToString($overAllTime),
                'Title' => 'Overall',
                'Line' => 1,
                'Empty' => 0.0001,
                'Color' => '3F51B5'
            );
            if (!empty($detailTime)) {
                foreach ($detailTime as $infoTableID => $time) {
                    $infoData = $infoTable->get($infoTableID);
                    $finalStats[] = array(
                        'Value' => $this->convertItervalToString($time),
                        'Title' => $infoData->name,
                        'Line' => round($time / $overAllTime, 4),
                        'Empty' => round(1.00001 - ($time / $overAllTime), 4),
                        'Color' => $infoData->color
                    );
                }
            }
            return $finalStats;
        }

        public function getDatesRange($type) {
            $dateStart = null;
            $dateEnd = null;
            $dateObj = new \Nette\Utils\DateTime();
            switch ($type) {
                case 'TWeek':
                    $year = $dateObj->format('Y');
                    $week = $dateObj->format('W');
                    $dateStart = $dateObj->setISODate($year, $week)->format('Y-m-d');
                    $dateEnd = $dateObj->modify('+6 days')->format('Y-m-d 23:59:59');
                    break;
                case 'LWeek' :
                    $dateObj->modify('-1 week');
                    $year = $dateObj->format('Y');
                    $week = $dateObj->format('W');
                    $dateStart = $dateObj->setISODate($year, $week)->format('Y-m-d');
                    $dateEnd = $dateObj->modify('+6 days')->format('Y-m-d 23:59:59');
                    break;
                case 'TMonth' :
                    $firstDay = new \Nette\Utils\DateTime('first day of this month');
                    $daysInMonth = $dateObj->format('t') - 1;
                    $dateStart = $firstDay->format('Y-m-d');
                    $dateEnd = $firstDay->modify('+' . $daysInMonth . ' days')->format('Y-m-d 23:59:59');
                    break;
                case 'LMonth' :
                    $dateObj->modify('-1 month');
                    $firstDay = new \Nette\Utils\DateTime('first day of last month');
                    $daysInMonth = $dateObj->format('t') - 1;
                    $dateStart = $firstDay->format('Y-m-d');
                    $dateEnd = $firstDay->modify('+' . $daysInMonth . ' days')->format('Y-m-d 23:59:59');
                    break;
            }

            return array($dateStart, $dateEnd);
        }

        public function convertItervalToString($interval) {
            return sprintf('%03d', $interval / self::HOUR) . ':' . sprintf('%02d', $interval / self::MINUTE % 60) . ':' . sprintf('%02d', $interval / self::SECOND % 60);
        }

    }
    