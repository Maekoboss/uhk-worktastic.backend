<?php

    namespace ApiModule;

    use \Drahak\Restful\IResource,
        \Drahak\Restful\Application\UI\ResourcePresenter;

    class UserPresenter extends BasePresenter {

        /**
         * @POST user/profile
         */
        public function actionRegistration() {
            try {
                $userWithEmail = $this->database->table('users')->where(
                                array(
                                    'active' => 1,
                                    'email' => $this->input->Email
                        ))->fetch();
                if ($userWithEmail != false) {
                    throw new Utils\ApiError('User with this email is already registered.', 400);
                }

                // Everything should be fine and user can be saved
                $password = $this->calculateHash($this->input->Password);
                $token = $this->calculateToken();
                $insertArray = array(
                    'active' => 1,
                    'auth_token' => $token,
                    'username' => $this->input->Email,
                    'password' => $password,
                    'email' => $this->input->Email,
                    'name' => $this->input->Name,
                );
                $userData = $this->database->table('users')->insert($insertArray);
                $this->moveRelatedData($userData);
                $this->resource->AuthToken = $token;
                $this->sendResource(IResource::JSON);
            } catch (Utils\ApiError $e) {
                $this->sendErrorResource($e);
            }
        }

        public function validateRegistration() {
            $this->input->field('Name')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Name is required');
            $this->input->field('Email')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Email is required')
                    ->addRule(\Drahak\Restful\Validation\IValidator::EMAIL, 'Email is at wrong format');
            $this->input->field('Password')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Password is required');
        }

        /**
         * @PUT user/profile
         */
        public function actionLogin() {
            try {
                $userWithEmail = $this->database->table('users')->where(
                                array(
                                    'active' => 1,
                                    'role' => 'registered',
                                    'email' => $this->input->Email
                        ))->fetch();
                if ($userWithEmail == false) {
                    throw new Utils\ApiError('User with this email does not exists.', 400);
                }

                // Everything should be fine and user can be saved
                $password = $this->calculateHash($this->input->Password);
                $user = $this->database->table('users')->where(
                                array(
                                    'username' => $this->input->Email,
                                    'password' => $password
                        ))->fetch();
                if ($user) {
                    $this->moveRelatedData($user);
                    $this->resource->UserItem = array(
                        'Name' => $user->name,
                        'Email' => $user->email,
                        'AuthToken' => $user->auth_token,
                    );
                    if ($user->companies) {
                        $this->resource->CompanyItem = array(
                            'Name' => $user->companies->name,
                        );
                    }
                    $this->sendResource(IResource::JSON);
                } else {
                    throw new Utils\ApiError('You entered wrong password', 400);
                }
            } catch (Utils\ApiError $e) {
                $this->sendErrorResource($e);
            }
        }

        public function validateLogin() {
            $this->input->field('Email')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Email is required')
                    ->addRule(\Drahak\Restful\Validation\IValidator::EMAIL, 'Email is at wrong format');
            $this->input->field('Password')
                    ->addRule(\Drahak\Restful\Validation\IValidator::REQUIRED, 'Password is required');
        }

        /**
         * @PUT user/company
         */
        public function actionCompany() {
            try {
                if (empty($this->user)) {
                    $error = new Utils\ApiError('You have to be logged in to do this.', 401);
                    $this->sendErrorResource($error);
                }

                $company = $this->database->table('companies')->where(array('pin' => $this->input->Pin, 'company_id' => $this->input->ID))->fetch();
                if (empty($company)) {
                    $error = new Utils\ApiError('We didn\'t find this company', 400);
                    $this->sendErrorResource($error);
                }

                // Assign company to user and save it to DB
                $now = new \Nette\Utils\DateTime();
                $this->user->update(array('companies_id' => $company->id, 'company_joined' => $now->format('Y-m-d')));

                // TODO: GET LOGO IF THERE IS ANY
                // Send response with Company detail
                $this->resource->CompanyItem = array(
                    'Name' => $company->name,
                );
                $this->sendResource(IResource::JSON);
            } catch (Utils\ApiError $e) {
                $this->sendErrorResource($e);
            }
        }

        /**
         * @DELETE user/company
         */
        public function actionCompanyDelete() {
            try {
                if (empty($this->user)) {
                    $error = new Utils\ApiError('You have to be logged in to do this.', 401);
                    $this->sendErrorResource($error);
                }

                $company = $this->user->companies;
                if (empty($company)) {
                    $error = new Utils\ApiError('We didn\'t find this company', 400);
                    $this->sendErrorResource($error);
                }

                // Detache company from user
                $this->user->update(array('companies_id' => null, 'company_joined' => null));
            } catch (Utils\ApiError $e) {
                $this->sendErrorResource($e);
            }
        }

        public function moveRelatedData($userData) {
            // Sessions
            $sessions = $this->database->table('sessions')->where(
                            array(
                                'users_id' => null,
                                'devices_id' => $this->device->id
                    ))->fetchAll();
            if ($sessions) {
                foreach ($sessions as $session) {
                    $session->update(array('users_id' => $userData->id));
                }
            }

            // Opened sessions
            $openedSession = $this->database->table('sessions_opened')->where(
                            array(
                                'devices_id' => $this->device->id
                    ))->fetchAll();
            if ($openedSession) {
                foreach ($openedSession as $session) {
                    $session->update(array('users_id' => $userData->id));
                }
            }
        }

        public function calculateHash($password) {
            return hash('sha512', $password);
        }

        public function calculateToken() {
            return bin2hex(openssl_random_pseudo_bytes(10));
        }

    }
    