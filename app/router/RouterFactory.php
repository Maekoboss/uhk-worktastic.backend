<?php

namespace App;

use Nette,
    Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\Application\Routers\SimpleRouter;

/**
 * Router factory.
 */
class RouterFactory {

    /**
     * @return \Nette\Application\IRouter
     */
    public function createRouter(\Nette\Database\Context $databaseObject) {

	// ziskani url a presenteru z db
	$database = $databaseObject;
	$uri = explode('/', $_SERVER['REQUEST_URI']);
	switch ($uri[1]) {
	    case 'en':
		$lang = 'en';
		break;
	    default:
		$lang = 'en';
		break;
	}
	$structure = $database->table('structure')->where('lang', $lang);
	$filterTable = array();
	if ($structure) {
	    foreach ($structure as $item) {
		if (($item->presenter != null || $item->presenter != '') && ($item->uri != null || $item->uri != '')) {
		    $presenterName = explode(':', $item->presenter);
		    $count = count($presenterName) - 2;
		    $filterTable[$item->uri] = $presenterName[$count];
		}
	    }
	}

	// ############################### ROUTER ###############################
	$router = new RouteList();

	//########## Backend ##############
	$router[] = $adminRouter = new RouteList('Admin');
	$adminRouter[] = new Route('<presenter>/<action>', 'Company:Homepage:default');

	return $router;
    }

}
