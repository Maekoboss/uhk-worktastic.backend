<?php

namespace App;

use Nette,
    Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\Application\Routers\SimpleRouter;

/**
 * Router factory.
 */
class SimpleRouterFactory {

    /**
     * @return \Nette\Application\IRouter
     */
    public function createRouter(\Nette\Database\Context $databaseObject) {
	
	// ############################### ROUTER ###############################
	$router = new SimpleRouter('Front:Homepage:default', Route::SECURED);

	return $router;
    }

}
