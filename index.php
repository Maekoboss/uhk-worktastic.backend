<?php

// HTTPS hack
    /*if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
	$_SERVER['SERVER_PORT'] = 443;
    }*/

// Let bootstrap create Dependency Injection container.
    $container = require __DIR__ . '/app/bootstrap.php';

// absolute filesystem path to the www root
    define('WWW_DIR', __DIR__);

// absolute filesystem path to the application root
    define('APP_DIR', __DIR__ . '/app');

// Run application.
    $container->getService('application')->run();
    