/*   
 Superfast CMS
 */

// Init tabs
var handleTabs = function () {
    if (location.hash !== '')
        $('a[href="' + location.hash + '"]').tab('show');

// remember the hash in the URL without jumping
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if (history.pushState) {
            history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
        } else {
            location.hash = '#' + $(e.target).attr('href').substr(1);
        }
    });
};

// Init load flash zpravy a jeji zobrazeni
var handleStartupFlashes = function () {
    $flashes = $('.flash-message');
    if ($flashes.length > 0) {
        $flashes.each(function () {
            $flashMessage = $(this);
            setTimeout(function () {
                showFlashMessage($flashMessage);
            }, 500);
        });
    }
};

// Zobrazeni flash zpravy
var showFlashMessage = function ($flasMessage) {
    $.gritter.add({
        title: $flasMessage.data('title'),
        text: $flasMessage.data('text'),
        sticky: false,
        time: '4000',
        class_name: 'gritter-' + $flasMessage.data('type')
    });
};

// Init sortable pluginu
var handleSortable = function () {
    // Return a helper with preserved width of cells
    var tableHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
            $(this).css('background', '#5da5e8');
        });
        return ui;
    };

    var sortableTable = $('.sortable tbody');
    if (sortableTable.length > 0) {
        var oldSortPosition;
        var newSortPosition;
        sortableTable.sortable({
            helper: tableHelper,
            placeholder: 'sortable-placeholder',
            handle: '.sort-btn',
            cancel: '',
            start: function (e, ui) {
                // creates a temporary attribute on the element with the old index
                $(this).attr('data-previndex', ui.item.index());
            },
            update: function (e, ui) {
                // gets the new and old index then removes the temporary attribute
                oldSortPosition = parseInt($(this).attr('data-previndex')) + 1;
                newSortPosition = ui.item.index() + 1;
                $(this).removeAttr('data-previndex');
                // Odesle sort formular
                if (oldSortPosition !== newSortPosition) {
                    var form = ui.item.find('form.formTools');
                    var toolInput = form.find('input.toolInput');
                    if (toolInput.length > 0) {
                        toolInput.val('sort');
                    }
                    var sortInput = form.find('input.sortInput');
                    if (sortInput.length > 0) {
                        sortInput.val(newSortPosition);
                        form.submit();
                    }
                }
            }
        }).disableSelection();
    }
};

//Init datetimepickeru
var handleDateTimePicker = function () {
    var dateInput = $('.dateInput, .monthInput');
    if (dateInput.length > 0) {
        lang = dateInput.find('input').data('lang');
        if (lang == 'cz' || lang == 'sk') {
            lang = 'cs';
        }
        if (dateInput.hasClass('dateInput')) {
            dateInput.datetimepicker({
                language: lang,
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        } else if (dateInput.hasClass('monthInput')) {
            dateInput.datetimepicker({
                viewMode: 'years',
                language: lang,
                format: 'YYYY-MM'
            });
        }
    }
};

// Init spusteni ajaxu a všech potřebných extenzí
var handleNetteAjax = function () {
    $.nette.ext('history').cache = false;
    // Hard redirect
    $.nette.ext('hardRedirect', {
        success: function (payload) {
            if (payload["hardRedirect"] && payload["link"]) {
                window.location.replace(payload["link"]);
            }
        }
    });
    // Form Content Modal
    $.nette.ext('initFormContentModal', {
        success: function (p, s, jq, settings) {
            $sender = settings.nette.el;
            if ($sender.data('toggle') === 'modal') {
                $modalWindow = $($sender.data('target'));
                $modalWindow.modal();
            }
        }
    });
    // Confirm modal
    $.nette.ext('confirm', {
        load: function () {
            $('.confirmModal').on('click', function (event) {
                var sender = $(this);
                var modalWindow = $('#confirmModal');
                if (modalWindow.length > 0) {
                    modalWindow.modal();
                    var yes = $("#" + modalWindow.attr('id') + " .yes");
                    var no = $("#" + modalWindow.attr('id') + " .no");
                    if (yes.length > 0) {
                        yes.on('click', function (e) {
                            form = sender.closest('form');
                            var toolInput = form.find('input.toolInput');
                            if (toolInput.length > 0) {
                                toolInput.val(sender.attr('name'));
                                form.submit();
                            }
                        });
                    }
                }
                return false;
            });
        },
        complete: function () {
            var modalWindow = $('#confirmModal');
            var yes = $("#" + modalWindow.attr('id') + " .yes");
            yes.unbind('click');
        }
    });

    // Gritter notifications for flash messages
    $.nette.ext('flashes', {
        error: function (jqXHR, status, error, settings) {
            $flashMessage = $($.parseHTML(
                    '<div data-type="danger" data-title="' + status + '" data-text="' + error + '"></div>'
                    ));
            showFlashMessage($flashMessage);
        },
        success: function (payload) {
            var flashSnippetName = 'snippet--flash';
            var flashSnippet = payload.snippets[flashSnippetName];
            if (flashSnippet !== undefined) {
                $flashSnippet = $($.parseHTML(flashSnippet));
                $flashSnippet.each(function (index) {
                    $this = $(this);
                    if ($this.hasClass('flash-message')) {
                        showFlashMessage($this);
                    }
                });
            }
        }
    });

    // Refresh all special inputs
    $.nette.ext('contentFormRefresh', {
        complete: function () {
            handleTabs();
            handleSortable();
            handleDateTimePicker();
        }
    });

    $.nette.init();
};

var Main = function () {
    "use strict";
    return {
        //main function
        init: function () {
            handleTabs();
            handleStartupFlashes();
            handleSortable();
            handleDateTimePicker();
            handleNetteAjax();
        }
    };
}();