-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `activities` (`id`, `name`, `color`) VALUES
(1,	'Working',	'4CAF50'),
(2,	'Meeting',	'03A9F4'),
(3,	'Moving',	'FF9800'),
(4,	'Resting',	'26A69A'),
(5,	'Eating',	'673AB7'),
(6,	'Smoking',	'E91E63');

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin` int(5) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `company_id` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `vat_id` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `companies` (`id`, `pin`, `name`, `street`, `city`, `zip`, `country`, `company_id`, `vat_id`) VALUES
(4,	77937,	'Univerzita Hradec Králové',	'Hradecká 1155',	'Hradec Králové',	'230 00',	'Czech Republic',	'554466',	'CZ554466');

DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `device_id` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `devices_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `devices` (`id`, `users_id`, `device_id`) VALUES
(8,	NULL,	'778e434f1d96fec3'),
(9,	NULL,	'PostMan111'),
(10,	NULL,	'Test11'),
(12,	NULL,	'Test12'),
(13,	NULL,	'tLqt2AnKnayQ6fAHIQURxg14nqs=');

DROP TABLE IF EXISTS `dev_json`;
CREATE TABLE `dev_json` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `feelings`;
CREATE TABLE `feelings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `feelings` (`id`, `name`, `color`) VALUES
(1,	'Good',	'4CAF50'),
(2,	'Excited',	'03A9F4'),
(3,	'Busy',	'FF9800'),
(4,	'Bored',	'26A69A'),
(5,	'Sick',	'673AB7'),
(6,	'Pissed off',	'E91E63');

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(4) COLLATE utf8_bin NOT NULL,
  `main_key` varchar(250) COLLATE utf8_bin NOT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `posts_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `file_name` varchar(250) COLLATE utf8_bin NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `structure_id` (`structure_id`),
  KEY `posts_id` (`posts_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `images_ibfk_1` FOREIGN KEY (`structure_id`) REFERENCES `structure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `images_ibfk_2` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `images_ibfk_4` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_key` varchar(250) COLLATE utf8_bin NOT NULL,
  `value` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `lang` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT 'cz',
  PRIMARY KEY (`id`),
  KEY `UQ_Options_name` (`main_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `options` (`id`, `main_key`, `value`, `lang`) VALUES
(1,	'siteName',	'Worktastic - better work tracking',	'en'),
(2,	'maintenance',	'0',	'en');

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL,
  `devices_id` int(11) DEFAULT NULL,
  `app_start` datetime DEFAULT NULL,
  `app_stop` datetime DEFAULT NULL,
  `start` timestamp NULL DEFAULT NULL,
  `stop` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  KEY `devices_id` (`devices_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `sessions_ibfk_4` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sessions_ibfk_2` FOREIGN KEY (`devices_id`) REFERENCES `devices` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `sessions_activities`;
CREATE TABLE `sessions_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessions_id` int(11) NOT NULL,
  `activities_id` int(11) NOT NULL DEFAULT '1',
  `start` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activities_id` (`activities_id`),
  KEY `sessions_id` (`sessions_id`),
  CONSTRAINT `sessions_activities_ibfk_3` FOREIGN KEY (`sessions_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sessions_activities_ibfk_2` FOREIGN KEY (`activities_id`) REFERENCES `activities` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `sessions_feelings`;
CREATE TABLE `sessions_feelings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessions_id` int(11) NOT NULL,
  `feelings_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feelings_id` (`feelings_id`),
  KEY `sessions_id` (`sessions_id`),
  CONSTRAINT `sessions_feelings_ibfk_3` FOREIGN KEY (`sessions_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sessions_feelings_ibfk_2` FOREIGN KEY (`feelings_id`) REFERENCES `feelings` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `sessions_opened`;
CREATE TABLE `sessions_opened` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL,
  `devices_id` int(11) DEFAULT NULL,
  `activities_id` int(11) NOT NULL,
  `feelings_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  KEY `activities_id` (`activities_id`),
  KEY `feelings_id` (`feelings_id`),
  KEY `devices_id` (`devices_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `sessions_opened_ibfk_7` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sessions_opened_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sessions_opened_ibfk_3` FOREIGN KEY (`activities_id`) REFERENCES `activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sessions_opened_ibfk_4` FOREIGN KEY (`feelings_id`) REFERENCES `feelings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sessions_opened_ibfk_5` FOREIGN KEY (`devices_id`) REFERENCES `devices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `structure`;
CREATE TABLE `structure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(250) COLLATE utf8_bin NOT NULL,
  `seo_title` varchar(160) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `uri` varchar(250) COLLATE utf8_bin NOT NULL,
  `status` int(1) NOT NULL,
  `lang` char(5) COLLATE utf8_bin NOT NULL DEFAULT 'cz',
  `main_key` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'mainMenu',
  `bPresenter` varchar(250) COLLATE utf8_bin NOT NULL,
  `bIcon` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `presenter` varchar(250) COLLATE utf8_bin NOT NULL,
  `layout` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT '@layout.latte',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_module` (`id`),
  KEY `menu_key` (`main_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `structure` (`id`, `parent_id`, `title`, `seo_title`, `description`, `uri`, `status`, `lang`, `main_key`, `bPresenter`, `bIcon`, `presenter`, `layout`, `sort`) VALUES
(1,	NULL,	'Company Profile',	'',	'',	'/',	2,	'en',	'companyAdmin',	':Admin:Company:Homepage:',	'fa fa-briefcase',	'',	'@layout.latte',	1),
(5,	NULL,	'Time Tracking',	'',	'',	'/',	2,	'en',	'companyAdmin',	':Admin:Company:Time:',	'fa fa-clock-o',	'',	'@layout.latte',	1);

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(55) COLLATE utf8_czech_ci NOT NULL DEFAULT 'frontend',
  `parent_id` int(11) DEFAULT NULL,
  `lang` varchar(4) COLLATE utf8_czech_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `v1` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `v2` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `v3` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_key` (`lang`,`key`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `translations_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `translations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `translations` (`id`, `module`, `parent_id`, `lang`, `key`, `v1`, `v2`, `v3`) VALUES
(1,	'backend',	NULL,	'cz',	'manage-your-site-without-pain',	'Upravujte své stránky snadno a rychle',	NULL,	NULL),
(2,	'backend',	NULL,	'cz',	'username',	'Uživatelské jméno',	NULL,	NULL),
(3,	'backend',	NULL,	'cz',	'password',	'Heslo',	NULL,	NULL),
(4,	'backend',	NULL,	'cz',	'log-in',	'Přihlásit se',	NULL,	NULL),
(5,	'backend',	NULL,	'cz',	'field-is-mandatory',	'Pole \"%label\" je povinné',	NULL,	NULL),
(6,	'backend',	NULL,	'cz',	'remember-me',	'Pamatuj si mě',	NULL,	NULL),
(7,	'backend',	NULL,	'cz',	'wrong-username',	'Zadaný uživatel neexistuje',	NULL,	NULL),
(8,	'backend',	NULL,	'cz',	'wrong-password',	'Zdali jste špatné heslo',	NULL,	NULL),
(9,	'backend',	NULL,	'cz',	'your-account-was-deactivated',	'Uživatel není aktivní',	NULL,	NULL),
(10,	'backend',	NULL,	'cz',	'frontend-view',	'Pohled na stránky',	NULL,	NULL),
(11,	'backend',	NULL,	'cz',	'log-out',	'Odhlásit se',	NULL,	NULL),
(12,	'backend',	NULL,	'cz',	'content',	'Obsah webu',	NULL,	NULL),
(13,	'backend',	NULL,	'cz',	'settings',	'Nastavení',	NULL,	NULL),
(14,	'backend',	NULL,	'cz',	'supported-formats-are-jpg-png-gif',	'Obrázek musí být ve formátu JPG, PNG nebo GIF',	NULL,	NULL),
(15,	'backend',	NULL,	'cz',	'max-file-size-is*NumberFix*',	'Maximální velikost souboru je 5MB',	NULL,	NULL),
(16,	'backend',	NULL,	'cz',	'save',	'Uložit',	NULL,	NULL),
(17,	'backend',	NULL,	'cz',	'save-new',	'Uložit a vložit další',	NULL,	NULL),
(18,	'backend',	NULL,	'cz',	'changes-were-saved',	'Změny byly uloženy',	NULL,	NULL),
(19,	'backend',	NULL,	'cz',	'item-was-deleted',	'Smazání bylo úspěšné',	NULL,	NULL),
(20,	'backend',	1,	'en',	'manage-your-site-without-pain',	'Manage your page without pain',	NULL,	NULL),
(22,	'backend',	2,	'en',	'username',	'Username',	NULL,	NULL),
(23,	'backend',	3,	'en',	'password',	'Password',	NULL,	NULL),
(24,	'backend',	4,	'en',	'log-in',	'Log in',	NULL,	NULL),
(25,	'backend',	5,	'en',	'field-is-mandatory',	'%label is mandatory field',	NULL,	NULL),
(26,	'backend',	6,	'en',	'remember-me',	'Remember me',	NULL,	NULL),
(27,	'backend',	7,	'en',	'wrong-username',	'Wrong username',	NULL,	NULL),
(28,	'backend',	8,	'en',	'wrong-password',	'Wrong password',	NULL,	NULL),
(29,	'backend',	9,	'en',	'your-account-was-deactivated',	'Your account was deactived',	NULL,	NULL),
(30,	'backend',	10,	'en',	'frontend-view',	'Frontend View',	NULL,	NULL),
(31,	'backend',	11,	'en',	'log-out',	'Log out',	NULL,	NULL),
(32,	'backend',	12,	'en',	'content',	'Content',	NULL,	NULL),
(33,	'backend',	13,	'en',	'settings',	'Settings',	NULL,	NULL),
(34,	'backend',	14,	'en',	'supported-formats-are-jpg-png-gif',	'Supported formats are JPG, PNG or GIF',	NULL,	NULL),
(35,	'backend',	15,	'en',	'max-file-size-is*NumberFix*',	'Max file size is 5MB',	NULL,	NULL),
(36,	'backend',	16,	'en',	'save',	'Save',	NULL,	NULL),
(37,	'backend',	17,	'en',	'save-new',	'Save and Add New',	NULL,	NULL),
(38,	'backend',	18,	'en',	'changes-were-saved',	'Changes were saved',	NULL,	NULL),
(39,	'backend',	19,	'en',	'item-was-deleted',	'Item was deleted',	NULL,	NULL),
(41,	'backend',	NULL,	'cz',	'success',	'Hotovo',	NULL,	NULL),
(43,	'backend',	41,	'en',	'success',	'Success',	NULL,	NULL),
(44,	'backend',	NULL,	'cz',	'info',	'Upozornění',	NULL,	NULL),
(45,	'backend',	44,	'en',	'info',	'Info',	NULL,	NULL),
(46,	'backend',	NULL,	'cz',	'danger',	'Chyba',	NULL,	NULL),
(47,	'backend',	46,	'en',	'danger',	'Error',	NULL,	NULL),
(48,	'backend',	NULL,	'cz',	'delete',	'Smazat',	NULL,	NULL),
(49,	'backend',	48,	'en',	'delete',	'Delete',	NULL,	NULL),
(50,	'backend',	NULL,	'cz',	'create-new',	'Vytvořit nový',	NULL,	NULL),
(51,	'backend',	50,	'en',	'create-new',	'Create New',	NULL,	NULL),
(52,	'backend',	NULL,	'cz',	'no-items',	'Žádné položky',	NULL,	NULL),
(53,	'backend',	52,	'en',	'no-items',	'No Items',	NULL,	NULL),
(54,	'backend',	NULL,	'cz',	'confirm-action',	'Potvrďte akci',	NULL,	NULL),
(55,	'backend',	54,	'en',	'confirm-action',	'Confirm Action',	NULL,	NULL),
(56,	'backend',	NULL,	'cz',	'close',	'Zavřít',	NULL,	NULL),
(57,	'backend',	56,	'en',	'close',	'Close',	NULL,	NULL),
(58,	'backend',	NULL,	'cz',	'do-you-really-want-to-take-action',	'Chcete skutečně provézt tuto akci?',	NULL,	NULL),
(59,	'backend',	58,	'en',	'do-you-really-want-to-take-action',	'Do you really want to take this action?',	NULL,	NULL),
(60,	'backend',	NULL,	'cz',	'yes',	'Ano',	NULL,	NULL),
(61,	'backend',	60,	'en',	'yes',	'Yes',	NULL,	NULL),
(62,	'backend',	NULL,	'cz',	'no',	'Ne',	NULL,	NULL),
(63,	'backend',	62,	'en',	'no',	'No',	NULL,	NULL),
(64,	'backend',	NULL,	'cz',	'more',	'Další',	NULL,	NULL),
(65,	'backend',	64,	'en',	'more',	'More',	NULL,	NULL),
(66,	'backend',	NULL,	'cz',	'you-was-logged-out-inactivity',	'Byl jste odhlášen z důvodu neaktivity. Přihlašte se prosím znovu.',	NULL,	NULL),
(67,	'backend',	66,	'en',	'you-was-logged-out-inactivity',	'You was logged out due to inactivity. Log in again.',	NULL,	NULL),
(68,	'backend',	NULL,	'cz',	'not-enough-permission',	'Pro vstup do této části webu nemáte dostatečné oprávnění.',	NULL,	NULL),
(69,	'backend',	68,	'en',	'not-enough-permission',	'You don\'t have enough permission to enter this part of the web.',	NULL,	NULL),
(72,	'backend',	NULL,	'cz',	'new-order-was-saved',	'Pořadí bylo upraveno',	NULL,	NULL),
(73,	'backend',	72,	'en',	'new-order-was-saved',	'New order was saved',	NULL,	NULL),
(74,	'backend',	NULL,	'cz',	'filter',	'Filtrovat',	NULL,	NULL),
(75,	'backend',	74,	'en',	'filter',	'Filter',	NULL,	NULL),
(103,	'backend',	NULL,	'cz',	'frontend',	'Veřejná část webu',	NULL,	NULL),
(104,	'backend',	103,	'en',	'frontend',	'Frontend',	NULL,	NULL),
(105,	'backend',	NULL,	'cz',	'backend',	'Administrační část',	NULL,	NULL),
(106,	'backend',	105,	'en',	'backend',	'Backend',	NULL,	''),
(107,	'backend',	NULL,	'cz',	'search-translations',	'Vyhledávání v překladech',	'',	''),
(108,	'backend',	107,	'en',	'search-translations',	'Search Translations',	'',	''),
(110,	'backend',	NULL,	'cz',	'language',	'Jazyk',	'',	''),
(111,	'backend',	110,	'en',	'language',	'Language',	'',	NULL),
(112,	'backend',	NULL,	'cz',	'one-house',	'Jeden dům',	'',	''),
(113,	'backend',	112,	'en',	'one-house',	'One house',	'',	NULL),
(114,	'backend',	NULL,	'cz',	'two-houses',	'Dva domy',	NULL,	NULL),
(115,	'backend',	114,	'en',	'two-houses',	'Two Houses',	NULL,	NULL),
(116,	'backend',	NULL,	'cz',	'ten-houses',	'Deset domů',	'',	''),
(117,	'backend',	116,	'en',	'ten-houses',	'Ten Houses',	'',	NULL),
(118,	'backend',	NULL,	'cz',	'translation-content',	'Obsah překladu',	'',	''),
(119,	'backend',	118,	'en',	'translation-content',	'Translation Content',	'',	NULL),
(120,	'backend',	NULL,	'cz',	'translation-key',	'Klíč překladu',	NULL,	NULL),
(121,	'backend',	120,	'en',	'translation-key',	'Translation Key',	NULL,	NULL),
(122,	'backend',	NULL,	'cz',	'language:',	'Jazyk',	NULL,	NULL),
(123,	'backend',	122,	'en',	'language:',	'Language:',	NULL,	NULL),
(124,	'backend',	NULL,	'cz',	'this-translation-key-already-exists',	'Daný překladový klíč již existuje. Použijte prosím jiný.',	'',	''),
(125,	'backend',	124,	'en',	'this-translation-key-already-exists',	'This translation key already exists. Please use different one.',	'',	NULL),
(126,	'backend',	NULL,	'cz',	'page-settings',	'Nastavení stránky',	'',	''),
(127,	'backend',	126,	'en',	'page-settings',	'Page Settings',	'',	NULL),
(128,	'backend',	NULL,	'cz',	'maintenance-mode',	'Mód údržby',	'',	''),
(129,	'backend',	128,	'en',	'maintenance-mode',	'Maintenance Mode',	'',	NULL),
(175,	'backend',	NULL,	'cz',	'error-appeared',	'Došlo k chybě!',	'',	''),
(176,	'backend',	175,	'en',	'error-appeared',	'Error Appeared!',	'',	NULL),
(177,	'backend',	NULL,	'cz',	'server-error-appeared',	'Došlo k chybě na serveru a požadavek se nepodařilo dokončit.',	'',	''),
(178,	'backend',	177,	'en',	'server-error-appeared',	'Server error appeared.',	'',	NULL),
(179,	'backend',	NULL,	'cz',	'requested-site-was-not-found',	'Požadovaná stránka nebyla nalezena.',	NULL,	NULL),
(180,	'backend',	179,	'en',	'requested-site-was-not-found',	'Requested site was not found.',	NULL,	NULL),
(181,	'backend',	NULL,	'cz',	'you-dont-have-permisson-for-this-page',	'K prohlížení tohoto obsahu nemáte dostatečná oprávnění.',	NULL,	NULL),
(182,	'backend',	181,	'en',	'you-dont-have-permisson-for-this-page',	'You don\'t have permission to see this content.',	NULL,	NULL),
(183,	'backend',	NULL,	'cz',	'back-to-homepage',	'Zpět na úvodní stránku',	NULL,	NULL),
(184,	'backend',	183,	'en',	'back-to-homepage',	'Back to Homepage',	NULL,	NULL),
(205,	'backend',	NULL,	'cz',	'images-were-uploaded',	'Obrázky byly nahrány',	NULL,	NULL),
(206,	'backend',	205,	'en',	'images-were-uploaded',	'Images were uploaded',	NULL,	NULL),
(207,	'backend',	NULL,	'cz',	'multiple-file-upload-help',	'[Nahrávání více souborů] Můžete vybrat několik souborů z jedné složky. Stačí držet <kbd>ctrl</kbd> na PC nebo <kbd>cmd</kbd> na MACU a klikat myší na soubory, které chcete přidat.',	NULL,	NULL),
(208,	'backend',	207,	'en',	'multiple-file-upload-help',	'[Multiple File Upload] You can upload several files from one folder. Just press and hold <kbd>ctrl</kbd> on PC or <kbd>cmd</kbd> on MAC and click with mouse on files you want to upload.',	NULL,	NULL),
(209,	'backend',	NULL,	'cz',	'upload',	'Nahrát',	NULL,	NULL),
(210,	'backend',	209,	'en',	'upload',	'Upload',	NULL,	NULL),
(211,	'backend',	NULL,	'cz',	'modal-dialog',	'Vyskakovací okno',	NULL,	NULL),
(212,	'backend',	211,	'en',	'modal-dialog',	'Modal Dialog',	NULL,	NULL),
(213,	'backend',	NULL,	'cz',	'registration',	'Registrovat',	NULL,	NULL),
(214,	'backend',	213,	'en',	'registration',	'Registration',	NULL,	NULL),
(218,	'backend',	NULL,	'cz',	'overall',	'Celkem',	NULL,	NULL),
(219,	'backend',	218,	'en',	'overall',	'Overall',	NULL,	NULL),
(220,	'backend',	NULL,	'cz',	'activities',	'Aktivity',	NULL,	NULL),
(221,	'backend',	220,	'en',	'activities',	'Activities',	NULL,	NULL),
(222,	'backend',	NULL,	'cz',	'company-administration',	'Administrace společnosti',	NULL,	NULL),
(223,	'backend',	222,	'en',	'company-administration',	'Company Administration',	NULL,	NULL),
(224,	'backend',	NULL,	'cz',	'passwords-must-match',	'Hesla se musí shodovat',	NULL,	NULL),
(225,	'backend',	224,	'en',	'passwords-must-match',	'Password must match',	NULL,	NULL),
(226,	'backend',	NULL,	'cz',	'name',	'Jméno',	NULL,	NULL),
(227,	'backend',	226,	'en',	'name',	'Name',	NULL,	NULL),
(228,	'backend',	NULL,	'cz',	'email',	'Email',	NULL,	NULL),
(229,	'backend',	228,	'en',	'email',	'Email',	NULL,	NULL),
(230,	'backend',	NULL,	'cz',	'password-again',	'Heslo znovu',	NULL,	NULL),
(231,	'backend',	230,	'en',	'password-again',	'Password again',	NULL,	NULL),
(232,	'backend',	NULL,	'cz',	'company-name',	'Název společnosti',	NULL,	NULL),
(233,	'backend',	232,	'en',	'company-name',	'Company name',	NULL,	NULL),
(234,	'backend',	NULL,	'cz',	'street',	'Ulice',	NULL,	NULL),
(235,	'backend',	234,	'en',	'street',	'Street',	NULL,	NULL),
(236,	'backend',	NULL,	'cz',	'city',	'Město',	NULL,	NULL),
(237,	'backend',	236,	'en',	'city',	'City',	NULL,	NULL),
(238,	'backend',	NULL,	'cz',	'zip',	'PSČ',	NULL,	NULL),
(239,	'backend',	238,	'en',	'zip',	'ZIP',	NULL,	NULL),
(240,	'backend',	NULL,	'cz',	'country',	'Země',	NULL,	NULL),
(241,	'backend',	240,	'en',	'country',	'Country',	NULL,	NULL),
(242,	'backend',	NULL,	'cz',	'company-id',	'IČO',	NULL,	NULL),
(243,	'backend',	242,	'en',	'company-id',	'Company ID',	NULL,	NULL),
(244,	'backend',	NULL,	'cz',	'vat-id',	'DIČ',	NULL,	NULL),
(245,	'backend',	244,	'en',	'vat-id',	'VAT ID',	NULL,	NULL),
(246,	'backend',	NULL,	'cz',	'this-company-id-is-used',	'Firma s tímto IČO už existuje',	NULL,	NULL),
(247,	'backend',	246,	'en',	'this-company-id-is-used',	'This Company ID is already in use',	NULL,	NULL),
(248,	'backend',	NULL,	'cz',	'this-user-alredy-exists',	'Uživatel s tímto emailem už existuje, pro registraci do administrace musíte použít jiný email, než který púoužíváte v aplikaci.',	NULL,	NULL),
(249,	'backend',	248,	'en',	'this-user-alredy-exists',	'User with this email already exists. You can\'t use same email for mobile app and administration',	NULL,	NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '0',
  `activekey` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `auth_token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT 'en',
  `username` varchar(250) COLLATE utf8_bin NOT NULL,
  `password` char(128) COLLATE utf8_bin NOT NULL,
  `email` varchar(250) COLLATE utf8_bin NOT NULL,
  `name` varchar(250) COLLATE utf8_bin NOT NULL,
  `role` varchar(250) COLLATE utf8_bin NOT NULL DEFAULT 'registered',
  `companies_id` int(11) DEFAULT NULL,
  `company_joined` date DEFAULT NULL,
  `users_data_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `users_data_id` (`users_data_id`),
  KEY `activekey` (`activekey`),
  KEY `auth_token` (`auth_token`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `users_ibfk_6` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`users_data_id`) REFERENCES `users_data` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `users` (`id`, `active`, `activekey`, `auth_token`, `lang`, `username`, `password`, `email`, `name`, `role`, `companies_id`, `company_joined`, `users_data_id`) VALUES
(1,	1,	NULL,	'35465hj56kh3jk',	'en',	'Admin',	'5699c10aeede0a6a0748b1750b2cf5e9d4e37e9d0a5169fabe776426e7c0b7581286c71b3d5629fed7a5c699805438839a52719960b5aad9501e74e8a9ff423b',	'mr@kapesnisvet.cz',	'Admin',	'superAdministrator',	NULL,	'0000-00-00',	NULL),
(16,	1,	NULL,	NULL,	'en',	'info@uhk.cz',	'024cd0ed8e8f2a0b55a57675dabe24212191c8a48a155ceb8320d0420410c66642e99739e9f33e83b6296430ca46262354732aa8e5a8dc5616623579362dde5c',	'info@uhk.cz',	'UHK Administrátor',	'companyAdmin',	4,	NULL,	NULL),
(18,	1,	NULL,	'7375b3445bf0213c3167',	'en',	'user@uhk.cz',	'024cd0ed8e8f2a0b55a57675dabe24212191c8a48a155ceb8320d0420410c66642e99739e9f33e83b6296430ca46262354732aa8e5a8dc5616623579362dde5c',	'user@uhk.cz',	'UHK User',	'registered',	4,	'2015-08-04',	NULL),
(19,	1,	NULL,	'sdfsdfsrfsdfd',	'en',	'petr@uhk.cz',	'024cd0ed8e8f2a0b55a57675dabe24212191c8a48a155ceb8320d0420410c66642e99739e9f33e83b6296430ca46262354732aa8e5a8dc5616623579362dde5c',	'petr@uhk.cz',	'Petr Tomíšek',	'registered',	4,	'2015-08-04',	NULL),
(20,	1,	NULL,	'rereerergg3167',	'en',	'jan@uhk.cz',	'024cd0ed8e8f2a0b55a57675dabe24212191c8a48a155ceb8320d0420410c66642e99739e9f33e83b6296430ca46262354732aa8e5a8dc5616623579362dde5c',	'jan@uhk.cz',	'Jan Potměšil',	'registered',	4,	'2015-08-04',	NULL);

DROP TABLE IF EXISTS `users_data`;
CREATE TABLE `users_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  CONSTRAINT `users_data_ibfk_2` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2015-08-04 19:00:04
